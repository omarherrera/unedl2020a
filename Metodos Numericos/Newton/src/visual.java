import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class visual {

    static double f(double x) {
        return (((5)*(Math.pow(x, 4))) -  (12) );
    }
    static double fdx(double x) { return ( ((20)*(Math.pow(x, 3)))); }

    private JButton btnRespuesta, btnLimpiar;
    private JTextField txtValorA, txtDec, txtPorc;
    private JLabel lblValorA, lblDec, lblPorc;
    private JTextArea txaRes;
    private KeyListener numeros, numeros2, numeros3, numeros4;
    private ActionListener actRes, actLimpiar;
    boolean bandera = false;

    public visual() {

        JFrame pnlMetodo = new JFrame("Metodo Newton-Raphson");
        pnlMetodo.setSize(400, 400);
        pnlMetodo.setVisible(true);
        pnlMetodo.setLayout(null);
        pnlMetodo.setDefaultCloseOperation(pnlMetodo.EXIT_ON_CLOSE);

        lblValorA = new JLabel("Valor del inicial: ");
        lblValorA.setBounds(20, 20, 150, 30);
        pnlMetodo.add(lblValorA);

        txtValorA = new JTextField();
        txtValorA.setBounds(200, 20, 150, 30);
        pnlMetodo.add(txtValorA);

        lblPorc = new JLabel("Porcentaje de error: ");
        lblPorc.setBounds(20, 60, 150, 30);
        pnlMetodo.add(lblPorc);

        txtPorc = new JTextField();
        txtPorc.setBounds(200, 60, 150, 30);
        pnlMetodo.add(txtPorc);

        lblDec = new JLabel("Decimales (0-9): ");
        lblDec.setBounds(20, 100, 150, 30);
        pnlMetodo.add(lblDec);

        txtDec = new JTextField();
        txtDec.setBounds(200, 100, 150, 30);
        pnlMetodo.add(txtDec);

        btnRespuesta = new JButton("Calcular");
        btnRespuesta.setBounds(200, 160, 100, 30);
        pnlMetodo.add(btnRespuesta);

        btnLimpiar = new JButton("Limpiar");
        btnLimpiar.setBounds(80, 160, 100, 30);
        pnlMetodo.add(btnLimpiar);

        txaRes = new JTextArea();
        txaRes.setBounds(20, 210, 345, 90);
        pnlMetodo.add(txaRes);

        JScrollPane scroll = new JScrollPane(txaRes);
        scroll.setBounds(20, 210, 345, 90);
        pnlMetodo.add(scroll);

        numeros = new KeyListener() {
            @Override
            public void keyTyped(KeyEvent k) {
                char car = k.getKeyChar();
                if (!(car>='0' && car<='9')) {
                    if (!(car == '.' )) {
                        if (!(car == '-' )) {
                            k.consume();
                            //JOptionPane.showMessageDialog(null, "Ingresa solo números.");
                        }
                    }
                }
                if(car == '.' && (txtValorA.getText().contains("."))){
                    k.consume();
                }
            }


            @Override
            public void keyPressed(KeyEvent keyEvent) {

            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {

            }
        };
        txtValorA.addKeyListener(numeros);

        numeros3 = new KeyListener() {
            @Override
            public void keyTyped(KeyEvent k) {
                char car = k.getKeyChar();
                if (!(car>='0' && car<='9')) {
                    if (!(car == '.')) {
                        k.consume();
                        //JOptionPane.showMessageDialog(null, "Ingresa solo números.");
                    }
                }
                if(car == '.' && (txtPorc.getText().contains("."))){
                    k.consume();
                }
            }


            @Override
            public void keyPressed(KeyEvent keyEvent) {

            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {

            }
        };
        txtPorc.addKeyListener(numeros3);

        numeros4 = new KeyListener() {
            @Override
            public void keyTyped(KeyEvent k) {
                char car = k.getKeyChar();
                if (!(car>='0' && car<='9')) {
                    /*if (!(car == '.')) {*/
                    k.consume();
                    //JOptionPane.showMessageDialog(null, "Ingresa solo números.");
                    //}
                }
                /*if(car == '.' && (txtValorB.getText().contains("."))){
                    k.consume();
                }*/
            }


            @Override
            public void keyPressed(KeyEvent keyEvent) {

            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {

            }
        };
        txtDec.addKeyListener(numeros4);

        actRes = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (bandera != true) {
                    String a1 = txtValorA.getText().trim();
                    if (a1.isEmpty()) {
                        JOptionPane.showMessageDialog(null, "El campo 1 no puede estar vacio.");
                    } else {
                        String p = txtPorc.getText().trim();
                        if (p.isEmpty()) {
                            JOptionPane.showMessageDialog(null, "El campo 2 no puede estar vacio.");
                        } else {
                            String d = txtDec.getText().trim();
                            if (d.isEmpty()) {
                                JOptionPane.showMessageDialog(null, "El campo 3 no puede estar vacio.");
                            } else {
                                double xi = Double.parseDouble(a1);
                                double e = Double.parseDouble(p);
                                int dec = Integer.parseInt(d), i = 0;
                                double x2;
                                double eR;
                                if (e <= 100) {
                                    if (dec >= 1 && dec <= 9) {
                                        bandera = true;
                                        if (dec == 1) {
                                            do {
                                                x2 = xi - (f(xi) / fdx(xi));
                                                eR = Math.abs((x2-xi)/x2)*100;
                                                txaRes.append(String.format("\nIteracion: %d    Aprox: %.1f     E. Rel: %.1f ", i+1, x2, eR));
                                                xi = x2;
                                                i++;
                                            }while(eR>=e);
                                        } else if (dec == 2) {
                                            do {
                                                x2 = xi - (f(xi) / fdx(xi));
                                                eR = Math.abs((x2-xi)/x2)*100;
                                                txaRes.append(String.format("\nIteracion: %d    Aprox: %.2f     E. Rel: %.2f ", i+1, x2, eR));
                                                xi = x2;
                                                i++;
                                            }while(eR>=e);
                                        } else if (dec == 3) {
                                            do {
                                                x2 = xi - (f(xi) / fdx(xi));
                                                eR = Math.abs((x2-xi)/x2)*100;
                                                txaRes.append(String.format("\nIteracion: %d    Aprox: %.3f     E. Rel: %.3f ", i+1, x2, eR));
                                                xi = x2;
                                                i++;
                                            }while(eR>=e);
                                        } else if (dec == 4) {
                                            do {
                                                x2 = xi - (f(xi) / fdx(xi));
                                                eR = Math.abs((x2-xi)/x2)*100;
                                                txaRes.append(String.format("\nIteracion: %d    Aprox: %.4f     E. Rel: %.4f ", i+1, x2, eR));
                                                xi = x2;
                                                i++;
                                            }while(eR>=e);
                                        } else if (dec == 5) {
                                            do {
                                                x2 = xi - (f(xi) / fdx(xi));
                                                eR = Math.abs((x2-xi)/x2)*100;
                                                txaRes.append(String.format("\nIteracion: %d    Aprox: %.5f     E. Rel: %.5f ", i+1, x2, eR));
                                                xi = x2;
                                                i++;
                                            }while(eR>=e);
                                        } else if (dec == 6) {
                                            do {
                                                x2 = xi - (f(xi) / fdx(xi));
                                                eR = Math.abs((x2-xi)/x2)*100;
                                                txaRes.append(String.format("\nIteracion: %d    Aprox: %.6f     E. Rel: %.6f ", i+1, x2, eR));
                                                xi = x2;
                                                i++;
                                            }while(eR>=e);
                                        } else if (dec == 7) {
                                            do {
                                                x2 = xi - (f(xi) / fdx(xi));
                                                eR = Math.abs((x2-xi)/x2)*100;
                                                txaRes.append(String.format("\nIteracion: %d    Aprox: %.7f     E. Rel: %.7f ", i+1, x2, eR));
                                                xi = x2;
                                                i++;
                                            }while(eR>=e);
                                        } else if (dec == 8) {
                                            do {
                                                x2 = xi - (f(xi) / fdx(xi));
                                                eR = Math.abs((x2-xi)/x2)*100;
                                                txaRes.append(String.format("\nIteracion: %d    Aprox: %.8f     E. Rel: %.8f ", i+1, x2, eR));
                                                xi = x2;
                                                i++;
                                            }while(eR>=e);
                                        } else if (dec == 9) {
                                            do {
                                                x2 = xi - (f(xi) / fdx(xi));
                                                eR = Math.abs((x2-xi)/x2)*100;
                                                txaRes.append(String.format("\nIteracion: %d    Aprox: %.9f     E. Rel: %.9f ", i+1, x2, eR));
                                                xi = x2;
                                                i++;
                                            }while(eR>=e);
                                        } else {
                                            JOptionPane.showMessageDialog(null, "Ingresa valores correctos.");
                                        }
                                    } else {
                                        JOptionPane.showMessageDialog(null, "Los valores deben ser entre 1 y 9.");
                                    }
                                } else {
                                    JOptionPane.showMessageDialog(null, "El error debe ser menor a 100.");
                                }
                            }
                        }
                    }
                }else{
                    JOptionPane.showMessageDialog(null, "Primero limpia la pantalla.");
                }
            }
        };
        btnRespuesta.addActionListener(actRes);

        actLimpiar = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                txtValorA.setText("");
                txtPorc.setText("");
                txtDec.setText("");
                txaRes.setText("");
                bandera=false;
            }
        };
        btnLimpiar.addActionListener(actLimpiar);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new visual();
            }
        });
    }

}