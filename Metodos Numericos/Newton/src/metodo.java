import java.util.Scanner;

public class metodo {

    static double f(double x) {
        return (((10)*(Math.pow(x, 3))) - ((4)*(Math.pow(x, 2))) + ((3)*(x)) - 1 );
    }
    static double fdx(double x) { return ( ((30)*(Math.pow(x, 2))) - ((8)*(x)) + 3 ); }

    public static void main(String args[]) {

        Scanner teclado = new Scanner(System.in);

        double xi, x2;
        double e, eR;
        int i=0, dec;

        System.out.println("Ingresa el valor inicial: ");
        xi = teclado.nextDouble();
        System.out.println("Ingresa el porcentaje de error: ");
        e = teclado.nextDouble();
        System.out.println("Ingresa el numero de decimales: ");
        dec = teclado.nextInt();

        do {

            x2 = xi - (f(xi) / fdx(xi));
            eR = Math.abs((x2-xi)/x2)*100;
            System.out.printf("\nIteracion: %d    Aprox: %.4f     E. Rel: %f ", i+1, x2, eR);
            xi = x2;
            i++;

        }while(eR>=e);

    }

}
