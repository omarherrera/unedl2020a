import java.util.Scanner;

public class mSecanteC {

    static double f(double x) { return (((Math.pow(x, 2))) + ((6)*(x)) + (9)); }

    public static void main(String args[]) {

        Scanner teclado = new Scanner(System.in);

        int i=0;
        double x0, xi, e, eR;
        double x2;
        System.out.println("Ingresa el valor de a: ");
        x0 = teclado.nextDouble();
        System.out.println("Ingresa el valor de b: ");
        xi = teclado.nextDouble();
        System.out.println("Ingresa el porcentaje de error: ");
        e = teclado.nextDouble();

        do {
            x2 = xi - ((f(xi) * (x0 - xi)) / (f(x0) - f(xi)));
            eR = Math.abs((x2 - xi) / (x2)) * 100;
            System.out.printf("\nIte: %d    x0: %f     xi: %f      f(x0): %f       f(xi): %f       Aprox: %f       E: %f", i + 1, x0, xi, f(x0), f(xi), x2, eR);
            x0 = xi;
            xi = x2;
            i++;
        }while(eR >= e);
    }
}
