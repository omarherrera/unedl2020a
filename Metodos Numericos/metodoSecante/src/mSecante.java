import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class mSecante {

    static double f(double x) {
        { return (((Math.pow(x, 2))) + ((6)*(x)) + (9)); }
    }

    private JButton btnRespuesta, btnLimpiar;
    private JTextField txtValorA, txtDec, txtPorc, txtValorB;
    private JLabel lblValorA, lblDec, lblPorc, lblValorB;
    private JTextArea txaRes;
    private KeyListener numeros, numeros2, numeros3, numeros4;
    private ActionListener actRes, actLimpiar;
    boolean bandera = false;

    public mSecante() {

        JFrame pnlMetodo = new JFrame("Metodo Secante");
        pnlMetodo.setSize(400, 400);
        pnlMetodo.setVisible(true);
        pnlMetodo.setLayout(null);
        pnlMetodo.setDefaultCloseOperation(pnlMetodo.EXIT_ON_CLOSE);

        lblValorA = new JLabel("Valor del intervalo A: ");
        lblValorA.setBounds(20, 20, 150, 30);
        pnlMetodo.add(lblValorA);

        txtValorA = new JTextField();
        txtValorA.setBounds(200, 20, 150, 30);
        pnlMetodo.add(txtValorA);

        lblValorB = new JLabel("Valor del intervalo B: ");
        lblValorB.setBounds(20, 67, 350, 30);
        pnlMetodo.add(lblValorB);

        txtValorB = new JTextField();
        txtValorB.setBounds(200, 65, 150, 30);
        pnlMetodo.add(txtValorB);

        lblPorc = new JLabel("Porcentaje de error: ");
        lblPorc.setBounds(20, 110, 150, 30);
        pnlMetodo.add(lblPorc);

        txtPorc = new JTextField();
        txtPorc.setBounds(200, 110, 150, 30);
        pnlMetodo.add(txtPorc);

        lblDec = new JLabel("Decimales (0-9): ");
        lblDec.setBounds(20, 150, 150, 30);
        pnlMetodo.add(lblDec);

        txtDec = new JTextField();
        txtDec.setBounds(200, 150, 150, 30);
        pnlMetodo.add(txtDec);

        btnRespuesta = new JButton("Calcular");
        btnRespuesta.setBounds(190, 190, 100, 30);
        pnlMetodo.add(btnRespuesta);

        btnLimpiar = new JButton("Limpiar");
        btnLimpiar.setBounds(80, 190, 100, 30);
        pnlMetodo.add(btnLimpiar);

        txaRes = new JTextArea();
        txaRes.setBounds(20, 230, 345, 90);
        pnlMetodo.add(txaRes);

        JScrollPane scroll = new JScrollPane(txaRes);
        scroll.setBounds(20, 230, 345, 90);
        pnlMetodo.add(scroll);

        numeros = new KeyListener() {
            @Override
            public void keyTyped(KeyEvent k) {
                char car = k.getKeyChar();
                if (!(car>='0' && car<='9')) {
                    if (!(car == '.' )) {
                        if (!(car == '-' )) {
                            k.consume();
                            //JOptionPane.showMessageDialog(null, "Ingresa solo números.");
                        }
                    }
                }
                if(car == '.' && (txtValorA.getText().contains("."))){
                    k.consume();
                }
            }


            @Override
            public void keyPressed(KeyEvent keyEvent) {

            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {

            }
        };
        txtValorA.addKeyListener(numeros);

        numeros2 = new KeyListener() {
            @Override
            public void keyTyped(KeyEvent k) {
                char car = k.getKeyChar();
                if (!(car>='0' && car<='9')) {
                    if (!(car == '.')) {
                        if (!(car == '-' )) {
                            k.consume();
                            //JOptionPane.showMessageDialog(null, "Ingresa solo números.");
                        }
                    }
                }
                /*if(car == '.' && (txtValorB.getText().contains("."))){
                    k.consume();
                }*/
            }


            @Override
            public void keyPressed(KeyEvent keyEvent) {

            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {

            }
        };
        txtValorB.addKeyListener(numeros2);

        numeros3 = new KeyListener() {
            @Override
            public void keyTyped(KeyEvent k) {
                char car = k.getKeyChar();
                if (!(car>='0' && car<='9')) {
                    if (!(car == '.')) {
                        k.consume();
                        //JOptionPane.showMessageDialog(null, "Ingresa solo números.");
                    }
                }
                if(car == '.' && (txtPorc.getText().contains("."))){
                    k.consume();
                }
            }


            @Override
            public void keyPressed(KeyEvent keyEvent) {

            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {

            }
        };
        txtPorc.addKeyListener(numeros3);

        numeros4 = new KeyListener() {
            @Override
            public void keyTyped(KeyEvent k) {
                char car = k.getKeyChar();
                if (!(car>='0' && car<='9')) {
                    /*if (!(car == '.')) {*/
                    k.consume();
                    //JOptionPane.showMessageDialog(null, "Ingresa solo números.");
                    //}
                }
                /*if(car == '.' && (txtValorB.getText().contains("."))){
                    k.consume();
                }*/
            }


            @Override
            public void keyPressed(KeyEvent keyEvent) {

            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {

            }
        };
        txtDec.addKeyListener(numeros4);

        actRes = actionEvent -> {
            if (!bandera) {
                String a1 = txtValorA.getText().trim();
                if (a1.isEmpty()) {
                    JOptionPane.showMessageDialog(null, "El campo 1 no puede estar vacio.");
                } else {
                    String b1 = txtValorB.getText().trim();
                    if (b1.isEmpty()) {
                        JOptionPane.showMessageDialog(null, "El campo 2 no puede estar vacio.");
                    } else {
                        String p = txtPorc.getText().trim();
                        if (p.isEmpty()) {
                            JOptionPane.showMessageDialog(null, "El campo 3 no puede estar vacio.");
                        } else {
                            String d = txtDec.getText().trim();
                            if (d.isEmpty()) {
                                JOptionPane.showMessageDialog(null, "El campo 4 no puede estar vacio.");
                            } else {
                                double x0 = Double.parseDouble(a1);
                                double xi = Double.parseDouble(b1);
                                double e = Double.parseDouble(p);
                                int dec = Integer.parseInt(d), i=0;
                                double x2, eR;

                                if (e <= 100) {
                                    if (dec >= 1 && dec <= 9) {
                                        if (dec == 1) {
                                            do {
                                                x2 = xi - ((f(xi) * (x0 - xi)) / (f(x0) - f(xi)));
                                                eR = Math.abs((x2 - xi) / (x2)) * 100;
                                                txaRes.append(String.format("\n%d     x0: %.1f        xi: %.1f        Aprox: %.1f         Error: %f", i, x0, xi, x2, eR));
                                                x0 = xi;
                                                xi = x2;
                                                i++;
                                            }while(eR >= e);
                                        } else if (dec == 2) {
                                            do {
                                                x2 = xi - ((f(xi) * (x0 - xi)) / (f(x0) - f(xi)));
                                                eR = Math.abs((x2 - xi) / (x2)) * 100;
                                                txaRes.append(String.format("\n%d     x0: %.2f        xi: %.2f        Aprox: %.2f         Error: %f", i, x0, xi, x2, eR));
                                                x0 = xi;
                                                xi = x2;
                                                i++;
                                            }while(eR >= e);
                                        } else if (dec == 3) {
                                            do {
                                                x2 = xi - ((f(xi) * (x0 - xi)) / (f(x0) - f(xi)));
                                                eR = Math.abs((x2 - xi) / (x2)) * 100;
                                                txaRes.append(String.format("\n%d     x0: %.3f        xi: %.3f        Aprox: %.3f         Error: %f", i, x0, xi, x2, eR));
                                                x0 = xi;
                                                xi = x2;
                                                i++;
                                            }while(eR >= e);
                                        } else if (dec == 4) {
                                            do {
                                                x2 = xi - ((f(xi) * (x0 - xi)) / (f(x0) - f(xi)));
                                                eR = Math.abs((x2 - xi) / (x2)) * 100;
                                                txaRes.append(String.format("\n%d     x0: %.4f        xi: %.4f        Aprox: %.4f         Error: %f", i, x0, xi, x2, eR));
                                                x0 = xi;
                                                xi = x2;
                                                i++;
                                            }while(eR >= e);
                                        } else if (dec == 5) {
                                            do {
                                                x2 = xi - ((f(xi) * (x0 - xi)) / (f(x0) - f(xi)));
                                                eR = Math.abs((x2 - xi) / (x2)) * 100;
                                                txaRes.append(String.format("\n%d     x0: %.5f        xi: %.5f        Aprox: %.5f         Error: %f", i, x0, xi, x2, eR));
                                                x0 = xi;
                                                xi = x2;
                                                i++;
                                            }while(eR >= e);
                                        } else if (dec == 6) {
                                            do {
                                                x2 = xi - ((f(xi) * (x0 - xi)) / (f(x0) - f(xi)));
                                                eR = Math.abs((x2 - xi) / (x2)) * 100;
                                                txaRes.append(String.format("\n%d     x0: %.6f        xi: %.6f        Aprox: %.6f         Error: %f", i, x0, xi, x2, eR));
                                                x0 = xi;
                                                xi = x2;
                                                i++;
                                            }while(eR >= e);
                                        } else if (dec == 7) {
                                            do {
                                                x2 = xi - ((f(xi) * (x0 - xi)) / (f(x0) - f(xi)));
                                                eR = Math.abs((x2 - xi) / (x2)) * 100;
                                                txaRes.append(String.format("\n%d     x0: %.7f        xi: %.7f        Aprox: %.7f         Error: %f", i, x0, xi, x2, eR));
                                                x0 = xi;
                                                xi = x2;
                                                i++;
                                            }while(eR >= e);
                                        } else if (dec == 8) {
                                            do {
                                                x2 = xi - ((f(xi) * (x0 - xi)) / (f(x0) - f(xi)));
                                                eR = Math.abs((x2 - xi) / (x2)) * 100;
                                                txaRes.append(String.format("\n%d     x0: %.8f        xi: %.8f        Aprox: %.8f         Error: %f", i, x0, xi, x2, eR));
                                                x0 = xi;
                                                xi = x2;
                                                i++;
                                            }while(eR >= e);
                                        } else if (dec == 9) {
                                            do {
                                                x2 = xi - ((f(xi) * (x0 - xi)) / (f(x0) - f(xi)));
                                                eR = Math.abs((x2 - xi) / (x2)) * 100;
                                                txaRes.append(String.format("\n%d     x0: %.9f        xi: %.9f        Aprox: %.9f         Error: %f", i, x0, xi, x2, eR));
                                                x0 = xi;
                                                xi = x2;
                                                i++;
                                            }while(eR >= e);
                                        } else {
                                            JOptionPane.showMessageDialog(null, "Ingresa valores correctos.");
                                        }
                                    } else {
                                        JOptionPane.showMessageDialog(null, "Los valores deben ser entre 1 y 9.");
                                    }
                                } else {
                                    JOptionPane.showMessageDialog(null, "El error debe ser menor a 100.");
                                }
                            }
                        }
                    }
                }
            }else{
                JOptionPane.showMessageDialog(null, "Primero limpia la pantalla.");
            }
        };
        btnRespuesta.addActionListener(actRes);

        actLimpiar = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                bandera = true;
                txtValorA.setText("");
                txtValorB.setText("");
                txtPorc.setText("");
                txtDec.setText("");
                txaRes.setText("");
            }
        };
        btnLimpiar.addActionListener(actLimpiar);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new mSecante();
            }
        });
    }

}