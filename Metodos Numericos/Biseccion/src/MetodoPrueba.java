import java.util.Scanner;

public class MetodoPrueba {

    static double f(double x) {
        return ( - ((5)*(Math.pow(x, 4))) + ((2)*(Math.pow(x,3))) + ((8)*(Math.pow(x,2))) - ((3)*(x)) - (12));
    }

    public static void main(String[] args) {

        double a, b, m = 0;
        int iteracion = 0;
        double error = 1;
        double mOld = 0;
        double porcError;

        Scanner leer = new Scanner(System.in);

        System.out.println("Ingrese el valor de A: ");
        a = leer.nextDouble();
        System.out.println("Ingrese el valor de B: ");
        b = leer.nextDouble();
        System.out.println("Ingrese el porcentaje de error: ");
        porcError = leer.nextFloat();

        /*if (f(a) * f(b) > 0) {
            System.out.println("La raíz no se encuentra en el intervalo");
        } else {*/
            do {
                m = (a + b) / 2;
                error = Math.abs((m - mOld) / m)*100;
                mOld = m;

                System.out.println("\nIteración: " + (iteracion+1));
                System.out.printf(String.format("m: %.3f\n", m));
                //System.out.printf(String.format("f(Pm): %.5f\n", f(m)));
                System.out.printf(String.format("Error abs: %.3f\n", error));

                //txaRes.append(String.format("\nIteracion: %i\na: %f\nb: %f\nm: %f\nf(a): %f\nf(b): %f\nf(Pm): %f Error absoluto: %f", iteracion, a, b, m, f(a), f(b), f(m), error;

                if (f(a) * f(m) > 0) {
                    a = m;
                } else {
                    b = m;
                }
                if (f(a) * f(m) == 0) {
                    System.out.println("Raiz exacta");
                }
                iteracion++;
            } while (porcError <= error);
        //}
    }
}
