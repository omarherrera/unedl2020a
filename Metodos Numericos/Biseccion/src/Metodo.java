import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class Metodo {

    static double f(double x) {
        return ( - ((5)*(Math.pow(x, 4))) + ((2)*(Math.pow(x,3))) + ((8)*(Math.pow(x,2))) - ((3)*(x)) - (12));
    }

    private JButton btnRespuesta, btnLimpiar;
    private JTextField txtValorA, txtDec, txtPorc, txtValorB;
    private JLabel lblValorA, lblDec, lblPorc, lblValorB;
    private JTextArea txaRes;
    private KeyListener numeros, numeros2, numeros3, numeros4;
    private ActionListener actRes, actLimpiar;
    boolean bandera = false;

    public Metodo() {

        JFrame pnlMetodo = new JFrame("Metodo Biseccion");
        pnlMetodo.setSize(400, 400);
        pnlMetodo.setVisible(true);
        pnlMetodo.setLayout(null);
        pnlMetodo.setDefaultCloseOperation(pnlMetodo.EXIT_ON_CLOSE);

        lblValorA = new JLabel("Valor del intervalo A: ");
        lblValorA.setBounds(20, 20, 150, 30);
        pnlMetodo.add(lblValorA);

        txtValorA = new JTextField();
        txtValorA.setBounds(200, 20, 150, 30);
        pnlMetodo.add(txtValorA);

        lblValorB = new JLabel("Valor del intervalo B: ");
        lblValorB.setBounds(20, 67, 350, 30);
        pnlMetodo.add(lblValorB);

        txtValorB = new JTextField();
        txtValorB.setBounds(200, 65, 150, 30);
        pnlMetodo.add(txtValorB);

        lblPorc = new JLabel("Porcentaje de error: ");
        lblPorc.setBounds(20, 110, 150, 30);
        pnlMetodo.add(lblPorc);

        txtPorc = new JTextField();
        txtPorc.setBounds(200, 110, 150, 30);
        pnlMetodo.add(txtPorc);

        lblDec = new JLabel("Decimales (0-9): ");
        lblDec.setBounds(20, 150, 150, 30);
        pnlMetodo.add(lblDec);

        txtDec = new JTextField();
        txtDec.setBounds(200, 150, 150, 30);
        pnlMetodo.add(txtDec);

        btnRespuesta = new JButton("Calcular");
        btnRespuesta.setBounds(190, 190, 100, 30);
        pnlMetodo.add(btnRespuesta);

        btnLimpiar = new JButton("Limpiar");
        btnLimpiar.setBounds(80, 190, 100, 30);
        pnlMetodo.add(btnLimpiar);

        txaRes = new JTextArea();
        txaRes.setBounds(20, 230, 345, 90);
        pnlMetodo.add(txaRes);

        JScrollPane scroll = new JScrollPane(txaRes);
        scroll.setBounds(20, 230, 345, 90);
        pnlMetodo.add(scroll);

        numeros = new KeyListener() {
            @Override
            public void keyTyped(KeyEvent k) {
                char car = k.getKeyChar();
                if (!(car>='0' && car<='9')) {
                    if (!(car == '.' )) {
                        if (!(car == '-' )) {
                            k.consume();
                            //JOptionPane.showMessageDialog(null, "Ingresa solo números.");
                        }
                    }
                }
                if(car == '.' && (txtValorA.getText().contains("."))){
                    k.consume();
                }
            }


            @Override
            public void keyPressed(KeyEvent keyEvent) {

            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {

            }
        };
        txtValorA.addKeyListener(numeros);

        numeros2 = new KeyListener() {
            @Override
            public void keyTyped(KeyEvent k) {
                char car = k.getKeyChar();
                if (!(car>='0' && car<='9')) {
                    if (!(car == '.')) {
                        if (!(car == '-' )) {
                        k.consume();
                        //JOptionPane.showMessageDialog(null, "Ingresa solo números.");
                        }
                    }
                }
                /*if(car == '.' && (txtValorB.getText().contains("."))){
                    k.consume();
                }*/
            }


            @Override
            public void keyPressed(KeyEvent keyEvent) {

            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {

            }
        };
        txtValorB.addKeyListener(numeros2);

        numeros3 = new KeyListener() {
            @Override
            public void keyTyped(KeyEvent k) {
                char car = k.getKeyChar();
                if (!(car>='0' && car<='9')) {
                    if (!(car == '.')) {
                        k.consume();
                        //JOptionPane.showMessageDialog(null, "Ingresa solo números.");
                    }
                }
                if(car == '.' && (txtPorc.getText().contains("."))){
                    k.consume();
                }
            }


            @Override
            public void keyPressed(KeyEvent keyEvent) {

            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {

            }
        };
        txtPorc.addKeyListener(numeros3);

        numeros4 = new KeyListener() {
            @Override
            public void keyTyped(KeyEvent k) {
                char car = k.getKeyChar();
                if (!(car>='0' && car<='9')) {
                    /*if (!(car == '.')) {*/
                    k.consume();
                    //JOptionPane.showMessageDialog(null, "Ingresa solo números.");
                    //}
                }
                /*if(car == '.' && (txtValorB.getText().contains("."))){
                    k.consume();
                }*/
            }


            @Override
            public void keyPressed(KeyEvent keyEvent) {

            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {

            }
        };
        txtDec.addKeyListener(numeros4);

        actRes = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (bandera != true) {
                    String a1 = txtValorA.getText().trim();
                    if (a1.isEmpty()) {
                        JOptionPane.showMessageDialog(null, "El campo 1 no puede estar vacio.");
                    } else {
                        String b1 = txtValorB.getText().trim();
                        if (b1.isEmpty()) {
                            JOptionPane.showMessageDialog(null, "El campo 2 no puede estar vacio.");
                        } else {
                            String p = txtPorc.getText().trim();
                            if (p.isEmpty()) {
                                JOptionPane.showMessageDialog(null, "El campo 3 no puede estar vacio.");
                            } else {
                                String d = txtDec.getText().trim();
                                if (d.isEmpty()) {
                                    JOptionPane.showMessageDialog(null, "El campo 4 no puede estar vacio.");
                                } else {
                                    double a = Double.parseDouble(a1);
                                    double b = Double.parseDouble(b1);
                                    double e = Double.parseDouble(p);
                                    int dec = Integer.parseInt(d);
                                    if (e <= 100) {
                                        if (dec >= 1 && dec <= 9) {
                                            double eR = 0, porcError = 0, m = 0, mOld = 0;
                                            int iteracion = 0;

                                            bandera = true;
                                            if (dec == 1) {
                                                if (f(a) * f(b) > 0) {
                                                    txaRes.append(String.format("La raíz no se encuentra en el intervalo"));
                                                } else {
                                                    do {
                                                        m = (a + b) / 2;
                                                        eR = Math.abs((m - mOld) / m)*100;
                                                        mOld = m;

                                                        txaRes.append(String.format("\nAproximaciion: %.1f  Error Relativo: %f", m, eR));

                                                        if (f(a) * f(m) > 0) {
                                                            a = m;
                                                        } else {
                                                            b = m;
                                                        }
                                                        if (f(a) * f(m) == 0) {
                                                            txaRes.append(String.format("Raiz exacta"));
                                                        }
                                                        iteracion++;
                                                    } while (porcError <= eR);
                                                }
                                            } else if (dec == 2) {
                                                if (f(a) * f(b) > 0) {
                                                    txaRes.append(String.format("La raíz no se encuentra en el intervalo"));
                                                } else {
                                                    do {
                                                        m = (a + b) / 2;
                                                        eR = Math.abs((m - mOld) / m)*100;
                                                        mOld = m;

                                                        txaRes.append(String.format("\nAproximaciion: %.2f  Error Relativo: %f", m, eR));

                                                        if (f(a) * f(m) > 0) {
                                                            a = m;
                                                        } else {
                                                            b = m;
                                                        }
                                                        if (f(a) * f(m) == 0) {
                                                            txaRes.append(String.format("Raiz exacta"));
                                                        }
                                                        iteracion++;
                                                    } while (porcError <= eR);
                                                }
                                            } else if (dec == 3) {
                                                if (f(a) * f(b) > 0) {
                                                    txaRes.append(String.format("La raíz no se encuentra en el intervalo"));
                                                } else {
                                                    do {
                                                        m = (a + b) / 2;
                                                        eR = Math.abs((m - mOld) / m)*100;
                                                        mOld = m;

                                                        txaRes.append(String.format("\nAproximaciion: %.3f  Error Relativo: %f", m, eR));

                                                        if (f(a) * f(m) > 0) {
                                                            a = m;
                                                        } else {
                                                            b = m;
                                                        }
                                                        if (f(a) * f(m) == 0) {
                                                            txaRes.append(String.format("Raiz exacta"));
                                                        }
                                                        iteracion++;
                                                    } while (porcError <= eR);
                                                }
                                            } else if (dec == 4) {
                                                if (f(a) * f(b) > 0) {
                                                    txaRes.append(String.format("La raíz no se encuentra en el intervalo"));
                                                } else {
                                                    do {
                                                        m = (a + b) / 2;
                                                        eR = Math.abs((m - mOld) / m)*100;
                                                        mOld = m;

                                                        txaRes.append(String.format("\nAproximaciion: %.4f  Error Relativo: %f", m, eR));

                                                        if (f(a) * f(m) > 0) {
                                                            a = m;
                                                        } else {
                                                            b = m;
                                                        }
                                                        if (f(a) * f(m) == 0) {
                                                            txaRes.append(String.format("Raiz exacta"));
                                                        }
                                                        iteracion++;
                                                    } while (porcError <= eR);
                                                }
                                            } else if (dec == 5) {
                                                if (f(a) * f(b) > 0) {
                                                    txaRes.append(String.format("La raíz no se encuentra en el intervalo"));
                                                } else {
                                                    do {
                                                        m = (a + b) / 2;
                                                        eR = Math.abs((m - mOld) / m)*100;
                                                        mOld = m;

                                                        txaRes.append(String.format("\nAproximaciion: %.5f  Error Relativo: %f", m, eR));

                                                        if (f(a) * f(m) > 0) {
                                                            a = m;
                                                        } else {
                                                            b = m;
                                                        }
                                                        if (f(a) * f(m) == 0) {
                                                            txaRes.append(String.format("Raiz exacta"));
                                                        }
                                                        iteracion++;
                                                    } while (porcError <= eR);
                                                }
                                            } else if (dec == 6) {
                                                if (f(a) * f(b) > 0) {
                                                    txaRes.append(String.format("La raíz no se encuentra en el intervalo"));
                                                } else {
                                                    do {
                                                        m = (a + b) / 2;
                                                        eR = Math.abs((m - mOld) / m)*100;
                                                        mOld = m;

                                                        txaRes.append(String.format("\nAproximaciion: %.6f  Error Relativo: %f", m, eR));

                                                        if (f(a) * f(m) > 0) {
                                                            a = m;
                                                        } else {
                                                            b = m;
                                                        }
                                                        if (f(a) * f(m) == 0) {
                                                            txaRes.append(String.format("Raiz exacta"));
                                                        }
                                                        iteracion++;
                                                    } while (porcError <= eR);
                                                }
                                            } else if (dec == 7) {
                                                if (f(a) * f(b) > 0) {
                                                    txaRes.append(String.format("La raíz no se encuentra en el intervalo"));
                                                } else {
                                                    do {
                                                        m = (a + b) / 2;
                                                        eR = Math.abs((m - mOld) / m)*100;
                                                        mOld = m;

                                                        txaRes.append(String.format("\nAproximaciion: %.7f  Error Relativo: %f", m, eR));

                                                        if (f(a) * f(m) > 0) {
                                                            a = m;
                                                        } else {
                                                            b = m;
                                                        }
                                                        if (f(a) * f(m) == 0) {
                                                            txaRes.append(String.format("Raiz exacta"));
                                                        }
                                                        iteracion++;
                                                    } while (porcError <= eR);
                                                }
                                            } else if (dec == 8) {
                                                if (f(a) * f(b) > 0) {
                                                    txaRes.append(String.format("La raíz no se encuentra en el intervalo"));
                                                } else {
                                                    do {
                                                        m = (a + b) / 2;
                                                        eR = Math.abs((m - mOld) / m)*100;
                                                        mOld = m;

                                                        txaRes.append(String.format("\nAproximaciion: %.8f  Error Relativo: %f", m, eR));

                                                        if (f(a) * f(m) > 0) {
                                                            a = m;
                                                        } else {
                                                            b = m;
                                                        }
                                                        if (f(a) * f(m) == 0) {
                                                            txaRes.append(String.format("Raiz exacta"));
                                                        }
                                                        iteracion++;
                                                    } while (porcError <= eR);
                                                }
                                            } else if (dec == 9) {
                                                if (f(a) * f(b) > 0) {
                                                    txaRes.append(String.format("La raíz no se encuentra en el intervalo"));
                                                } else {
                                                    do {
                                                        m = (a + b) / 2;
                                                        eR = Math.abs((m - mOld) / m)*100;
                                                        mOld = m;

                                                        txaRes.append(String.format("\nAproximaciion: %.9f  Error Relativo: %f", m, eR));

                                                        if (f(a) * f(m) > 0) {
                                                            a = m;
                                                        } else {
                                                            b = m;
                                                        }
                                                        if (f(a) * f(m) == 0) {
                                                            txaRes.append(String.format("Raiz exacta"));
                                                        }
                                                        iteracion++;
                                                    } while (porcError <= eR);
                                                }
                                            } else {
                                                JOptionPane.showMessageDialog(null, "Ingresa valores correctos.");
                                            }
                                        } else {
                                            JOptionPane.showMessageDialog(null, "Los valores deben ser entre 1 y 9.");
                                        }
                                    } else {
                                        JOptionPane.showMessageDialog(null, "El error debe ser menor a 100.");
                                    }
                                }
                            }
                        }
                    }
                }else{
                    JOptionPane.showMessageDialog(null, "Primero limpia la pantalla.");
                }
            }
        };
        btnRespuesta.addActionListener(actRes);

        actLimpiar = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                txtValorA.setText("");
                txtValorB.setText("");
                txtPorc.setText("");
                txtDec.setText("");
                txaRes.setText("");
            }
        };
        btnLimpiar.addActionListener(actLimpiar);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Metodo();
            }
        });
    }

}

