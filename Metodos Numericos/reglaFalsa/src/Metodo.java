import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class Metodo {

    static double f(double x) {
        return ( ((0.2)*(Math.pow(x, 3))) + ((6)*(Math.pow(x, 2))) + ((3.2)*(x)) - (5)   );
    }

    private JButton btnRespuesta, btnLimpiar;
    private JTextField txtValorA, txtDec, txtPorc, txtValorB;
    private JLabel lblValorA, lblDec, lblPorc, lblValorB;
    private JTextArea txaRes;
    private KeyListener numeros, numeros2, numeros3, numeros4;
    private ActionListener actRes, actLimpiar;
    boolean bandera = false;

    public Metodo() {

        JFrame pnlMetodo = new JFrame("Metodo Regla Falsa");
        pnlMetodo.setSize(400, 400);
        pnlMetodo.setVisible(true);
        pnlMetodo.setLayout(null);
        pnlMetodo.setDefaultCloseOperation(pnlMetodo.EXIT_ON_CLOSE);

        lblValorA = new JLabel("Valor del intervalo A: ");
        lblValorA.setBounds(20, 20, 150, 30);
        pnlMetodo.add(lblValorA);

        txtValorA = new JTextField();
        txtValorA.setBounds(200, 20, 150, 30);
        pnlMetodo.add(txtValorA);

        lblValorB = new JLabel("Valor del intervalo B: ");
        lblValorB.setBounds(20, 67, 350, 30);
        pnlMetodo.add(lblValorB);

        txtValorB = new JTextField();
        txtValorB.setBounds(200, 65, 150, 30);
        pnlMetodo.add(txtValorB);

        lblPorc = new JLabel("Porcentaje de error: ");
        lblPorc.setBounds(20, 110, 150, 30);
        pnlMetodo.add(lblPorc);

        txtPorc = new JTextField();
        txtPorc.setBounds(200, 110, 150, 30);
        pnlMetodo.add(txtPorc);

        lblDec = new JLabel("Decimales (0-9): ");
        lblDec.setBounds(20, 150, 150, 30);
        pnlMetodo.add(lblDec);

        txtDec = new JTextField();
        txtDec.setBounds(200, 150, 150, 30);
        pnlMetodo.add(txtDec);

        btnRespuesta = new JButton("Calcular");
        btnRespuesta.setBounds(190, 190, 100, 30);
        pnlMetodo.add(btnRespuesta);

        btnLimpiar = new JButton("Limpiar");
        btnLimpiar.setBounds(80, 190, 100, 30);
        pnlMetodo.add(btnLimpiar);

        txaRes = new JTextArea();
        txaRes.setBounds(20, 230, 345, 90);
        pnlMetodo.add(txaRes);

        JScrollPane scroll = new JScrollPane(txaRes);
        scroll.setBounds(20, 230, 345, 90);
        pnlMetodo.add(scroll);

        numeros = new KeyListener() {
            @Override
            public void keyTyped(KeyEvent k) {
                char car = k.getKeyChar();
                if (!(car>='0' && car<='9')) {
                    if (!(car == '.' )) {
                        if (!(car == '-' )) {
                            k.consume();
                            //JOptionPane.showMessageDialog(null, "Ingresa solo números.");
                        }
                    }
                }
                if(car == '.' && (txtValorA.getText().contains("."))){
                    k.consume();
                }
            }


            @Override
            public void keyPressed(KeyEvent keyEvent) {

            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {

            }
        };
        txtValorA.addKeyListener(numeros);

        numeros2 = new KeyListener() {
            @Override
            public void keyTyped(KeyEvent k) {
                char car = k.getKeyChar();
                if (!(car>='0' && car<='9')) {
                    if (!(car == '.')) {
                        if (!(car == '-' )) {
                            k.consume();
                            //JOptionPane.showMessageDialog(null, "Ingresa solo números.");
                        }
                    }
                }
                if(car == '.' && (txtValorB.getText().contains("."))){
                    k.consume();
                }
            }


            @Override
            public void keyPressed(KeyEvent keyEvent) {

            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {

            }
        };
        txtValorB.addKeyListener(numeros2);

        numeros3 = new KeyListener() {
            @Override
            public void keyTyped(KeyEvent k) {
                char car = k.getKeyChar();
                if (!(car>='0' && car<='9')) {
                    if (!(car == '.')) {
                        k.consume();
                        //JOptionPane.showMessageDialog(null, "Ingresa solo números.");
                    }
                }
                if(car == '.' && (txtPorc.getText().contains("."))){
                    k.consume();
                }
            }


            @Override
            public void keyPressed(KeyEvent keyEvent) {

            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {

            }
        };
        txtPorc.addKeyListener(numeros3);

        numeros4 = new KeyListener() {
            @Override
            public void keyTyped(KeyEvent k) {
                char car = k.getKeyChar();
                if (!(car>='0' && car<='9')) {
                    /*if (!(car == '.')) {*/
                    k.consume();
                    //JOptionPane.showMessageDialog(null, "Ingresa solo números.");
                    //}
                }
                if(car == '.' && (txtValorB.getText().contains("."))){
                    k.consume();
                }
            }


            @Override
            public void keyPressed(KeyEvent keyEvent) {

            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {

            }
        };
        txtDec.addKeyListener(numeros4);

        actRes = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (bandera != true) {
                    String a1 = txtValorA.getText().trim();
                    if (a1.isEmpty()) {
                        JOptionPane.showMessageDialog(null, "El campo 1 no puede estar vacio.");
                    } else {
                        String b1 = txtValorB.getText().trim();
                        if (b1.isEmpty()) {
                            JOptionPane.showMessageDialog(null, "El campo 2 no puede estar vacio.");
                        } else {
                            String p = txtPorc.getText().trim();
                            if (p.isEmpty()) {
                                JOptionPane.showMessageDialog(null, "El campo 3 no puede estar vacio.");
                            } else {
                                String d = txtDec.getText().trim();
                                if (d.isEmpty()) {
                                    JOptionPane.showMessageDialog(null, "El campo 4 no puede estar vacio.");
                                } else {
                                    double a = Double.parseDouble(a1);
                                    double b = Double.parseDouble(b1);
                                    double e = Double.parseDouble(p);
                                    int dec = Integer.parseInt(d), i=0;
                                    double xR, xRN;
                                    double eR;
                                    if (e <= 100) {
                                        if (dec >= 1 && dec <= 9) {
                                            bandera = true;
                                            if (dec == 1) {
                                                xR=(f(a)*b - f(b)*a)/ (f(a)-f(b));
                                                a=xR;
                                                i=0;
                                                System.out.printf("xR:%f",xR);

                                                if(f(a)*f(xR)==0) {
                                                    System.out.printf("La raiz es: %f", b);
                                                } else if(f(a)*f(xR)<0) {
                                                    b=xR;
                                                } else {
                                                    do {
                                                        xRN = b - ((f(b) * a - f(b) * b) / (f(a) - f(b)));
                                                        eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                        txaRes.append(String.format("\n%d   Raiz: %.1f     Error rel: %.1f", i+1, xRN, eR));
                                                        a = xRN;
                                                        xR = xRN;
                                                        i++;
                                                    } while (eR >= e);
                                                }
                                            } else if (dec == 2) {
                                                xR=(f(a)*b - f(b)*a)/ (f(a)-f(b));
                                                a=xR;
                                                i=0;
                                                System.out.printf("xR:%f",xR);

                                                if(f(a)*f(xR)==0) {
                                                    System.out.printf("La raiz es: %f", b);
                                                } else if(f(a)*f(xR)<0) {
                                                    b=xR;
                                                } else {
                                                    do {
                                                        xRN = b - ((f(b) * a - f(b) * b) / (f(a) - f(b)));
                                                        eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                        txaRes.append(String.format("\n%d   Raiz: %.2f     Error rel: %.2f", i+1, xRN, eR));
                                                        a = xRN;
                                                        xR = xRN;
                                                        i++;
                                                    } while (eR >= e);
                                                }
                                            } else if (dec == 3) {
                                                xR=(f(a)*b - f(b)*a)/ (f(a)-f(b));
                                                a=xR;
                                                i=0;
                                                System.out.printf("xR:%f",xR);

                                                if(f(a)*f(xR)==0) {
                                                    System.out.printf("La raiz es: %f", b);
                                                } else if(f(a)*f(xR)<0) {
                                                    b=xR;
                                                } else {
                                                    do {
                                                        xRN = b - ((f(b) * a - f(b) * b) / (f(a) - f(b)));
                                                        eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                        txaRes.append(String.format("\n%d   Raiz: %.3f     Error rel: %.3f", i+1, xRN, eR));
                                                        a = xRN;
                                                        xR = xRN;
                                                        i++;
                                                    } while (eR >= e);
                                                }
                                            } else if (dec == 4) {
                                                xR=(f(a)*b - f(b)*a)/ (f(a)-f(b));
                                                a=xR;
                                                i=0;
                                                System.out.printf("xR:%f",xR);

                                                if(f(a)*f(xR)==0) {
                                                    System.out.printf("La raiz es: %f", b);
                                                } else if(f(a)*f(xR)<0) {
                                                    b=xR;
                                                } else {
                                                    do {
                                                        xRN = b - ((f(b) * a - f(b) * b) / (f(a) - f(b)));
                                                        eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                        txaRes.append(String.format("\n%d   Raiz: %.4f     Error rel: %.4f", i+1, xRN, eR));
                                                        a = xRN;
                                                        xR = xRN;
                                                        i++;
                                                    } while (eR >= e);
                                                }
                                            } else if (dec == 5) {
                                                xR=(f(a)*b - f(b)*a)/ (f(a)-f(b));
                                                a=xR;
                                                i=0;
                                                System.out.printf("xR:%f",xR);

                                                if(f(a)*f(xR)==0) {
                                                    System.out.printf("La raiz es: %f", b);
                                                } else if(f(a)*f(xR)<0) {
                                                    b=xR;
                                                } else {
                                                    do {
                                                        xRN = b - ((f(b) * a - f(b) * b) / (f(a) - f(b)));
                                                        eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                        txaRes.append(String.format("\n%d   Raiz: %.5f     Error rel: %.5f", i+1, xRN, eR));
                                                        a = xRN;
                                                        xR = xRN;
                                                        i++;
                                                    } while (eR >= e);
                                                }
                                            } else if (dec == 6) {
                                                xR=(f(a)*b - f(b)*a)/ (f(a)-f(b));
                                                a=xR;
                                                i=0;
                                                System.out.printf("xR:%f",xR);

                                                if(f(a)*f(xR)==0) {
                                                    System.out.printf("La raiz es: %f", b);
                                                } else if(f(a)*f(xR)<0) {
                                                    b=xR;
                                                } else {
                                                    do {
                                                        xRN = b - ((f(b) * a - f(b) * b) / (f(a) - f(b)));
                                                        eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                        txaRes.append(String.format("\n%d   Raiz: %.6f     Error rel: %.6f", i+1, xRN, eR));
                                                        a = xRN;
                                                        xR = xRN;
                                                        i++;
                                                    } while (eR >= e);
                                                }
                                            } else if (dec == 7) {
                                                xR=(f(a)*b - f(b)*a)/ (f(a)-f(b));
                                                a=xR;
                                                i=0;
                                                System.out.printf("xR:%f",xR);

                                                if(f(a)*f(xR)==0) {
                                                    System.out.printf("La raiz es: %f", b);
                                                } else if(f(a)*f(xR)<0) {
                                                    b=xR;
                                                } else {
                                                    do {
                                                        xRN = b - ((f(b) * a - f(b) * b) / (f(a) - f(b)));
                                                        eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                        txaRes.append(String.format("\n%d   Raiz: %.7f     Error rel: %.7f", i+1, xRN, eR));
                                                        a = xRN;
                                                        xR = xRN;
                                                        i++;
                                                    } while (eR >= e);
                                                }
                                            } else if (dec == 8) {
                                                xR=(f(a)*b - f(b)*a)/ (f(a)-f(b));
                                                a=xR;
                                                i=0;
                                                System.out.printf("xR:%f",xR);

                                                if(f(a)*f(xR)==0) {
                                                    System.out.printf("La raiz es: %f", b);
                                                } else if(f(a)*f(xR)<0) {
                                                    b=xR;
                                                } else {
                                                    do {
                                                        xRN = b - ((f(b) * a - f(b) * b) / (f(a) - f(b)));
                                                        eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                        txaRes.append(String.format("\n%d   Raiz: %.8f     Error rel: %.8f", i+1, xRN, eR));
                                                        a = xRN;
                                                        xR = xRN;
                                                        i++;
                                                    } while (eR >= e);
                                                }
                                            } else if (dec == 9) {
                                                xR=(f(a)*b - f(b)*a)/ (f(a)-f(b));
                                                a=xR;
                                                i=0;
                                                System.out.printf("xR:%f",xR);

                                                if(f(a)*f(xR)==0) {
                                                    System.out.printf("La raiz es: %f", b);
                                                } else if(f(a)*f(xR)<0) {
                                                    b=xR;
                                                } else {
                                                    do {
                                                        xRN = b - ((f(b) * a - f(b) * b) / (f(a) - f(b)));
                                                        eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                        txaRes.append(String.format("\n%d   Raiz: %.9f     Error rel: %.9f", i+1, xRN, eR));
                                                        a = xRN;
                                                        xR = xRN;
                                                        i++;
                                                    } while (eR >= e);
                                                }
                                            } else {
                                                JOptionPane.showMessageDialog(null, "Ingresa valores correctos.");
                                            }
                                        } else {
                                            JOptionPane.showMessageDialog(null, "Los valores deben ser entre 1 y 9.");
                                        }
                                    } else {
                                        JOptionPane.showMessageDialog(null, "El error debe ser menor a 100.");
                                    }
                                }
                            }
                        }
                    }
                }else{
                    JOptionPane.showMessageDialog(null, "Primero limpia la pantalla.");
                }
            }
        };
        btnRespuesta.addActionListener(actRes);

        actLimpiar = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                txtValorA.setText("");
                txtValorB.setText("");
                txtPorc.setText("");
                txtDec.setText("");
                txaRes.setText("");
                bandera=false;
            }
        };
        btnLimpiar.addActionListener(actLimpiar);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Metodo();
            }
        });
    }

}