import javax.swing.*;
import java.util.Scanner;

public class MetodoC {

    static double f(double x) {
        return (((Math.pow(x, 3))) - (x) + (2));
    }

    public static void main(String args[]) {

        Scanner teclado = new Scanner(System.in);

        double a, b, e = 0, eR;
        double xR, xRN = 0;
        int i;
        System.out.println("Ingresa el valor de a: ");
        a = teclado.nextDouble();
        System.out.println("Ingresa el valor de b: ");
        b = teclado.nextDouble();
        System.out.println("Ingresa el porcentaje de error: ");
        e = teclado.nextDouble();

        xR=(f(a)*b - f(b)*a)/ (f(a)-f(b));
        a=xR;
        i=0;
        System.out.printf("xR:%f",xR);

        if(f(a)*f(xR)==0) {
            System.out.printf("La raiz es: %f", b);
        } else if(f(a)*f(xR)<0) {
            b=xR;
        } else {
            do {
                xRN = b - ((f(b) * a - f(b) * b) / (f(a) - f(b)));
                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                System.out.printf("\nIteracion:%d  XRN: %f    Error Relativo: %f", i, xRN, eR);
                a = xRN;
                xR = xRN;
                i++;
            } while (eR >= e);
        }

        JOptionPane.showMessageDialog(null, "El intervalo excede las 100 iteraciones.");

    }
}
