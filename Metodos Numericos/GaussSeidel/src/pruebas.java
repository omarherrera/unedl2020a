public class pruebas
{
    public static void main(String[] args)
    {
        double[] Uno = {
                3,-0.1,-0.2,7.85
        };
        double[] Dos = {
                0.1,7,-0.3,-19.3
        };
        double[] Tres = {
                0.3,-0.2,10,71.4
        };

        double[] FUno = {Uno[1]-1,Uno[2]-1,Uno[3]};
        double[] FDos = {Dos[0]-1,Dos[2]-1,Dos[3]};
        double[] FTres = {Tres[0]-1,Tres[1]-1,Tres[3]};



        double[] variables = {
                0,0,0
        };

        //Ecuacion e = new Ecuacion();

        int cont = 1;
        double error= 0.1;
        double errorA = 100;
        double errorB = 100;
        double errorC = 100;

        do{
            variables[0] = ((FUno[0] * variables[1]) + (FUno[1] * variables[2]) + (FUno[2]))/Uno[0];
            variables[1] = ((FDos[0] * variables[0]) + (FDos[1] * variables[2]) + (FDos[2]))/Dos[1];
            variables[2] = ((FTres[0] * variables[0]) + (FTres[1] * variables[1]) + (FTres[2]))/Tres[2];

            double a = (Uno[0]*variables[0])
                    + (Uno[1]*variables[1])
                    + (Uno[2]*variables[2]);
            double b = (Dos[0]*variables[0])
                    + (Dos[1]*variables[1])
                    + (Dos[2]*variables[2]);
            double c = (Tres[0]*variables[0])
                    + (Tres[1]*variables[1])
                    + (Tres[2]*variables[2]);

            System.out.println("Iteracion " + cont);
            System.out.println();

            for(int i=0;i<3;i++)
            {
                System.out.println(variables[i]);
            }
            System.out.println();

            errorA = Math.abs((Uno[3] - a )/ Uno[3])*100;
            System.out.println(errorA);
            errorB = Math.abs((Dos[3] - b )/ Dos[3])*100;
            System.out.println(errorB);
            errorC = Math.abs((Tres[3]-c)/ Tres[3])*100;
            System.out.println(errorC);


            cont++;

        }while(error<errorA || error<errorB || error<errorC);


        System.out.println("las raices son:\n"
                +variables[0] +"x\n"+ variables[1]+"y\n" +
                variables[2] + "z\n");
    }
}