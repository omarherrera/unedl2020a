import java.util.Scanner;

public class metodo {


    public static void main(String[] args) {

        double x1, y1, z1, d1;
        double x2, y2, z2, d2;
        double x3, y3, z3, d3;
        double fx1=0, fx2=0, fx3=0;
        double fvR, fvR2, fvR3;
        double eR, eR2, eR3, porcE;
        int i=0;

        Scanner teclado = new Scanner(System.in);

        System.out.println("-Primera ecuación-");
        System.out.println("Ingresa el valor que acompaña a X:");
        x1 = teclado.nextDouble();
        System.out.println("Ingresa el valor que acompaña a Y:");
        y1 = teclado.nextDouble();
        System.out.println("Ingresa el valor que acompaña a Z:");
        z1 = teclado.nextDouble();
        System.out.println("Ingresa el valor del resultado:");
        d1 = teclado.nextDouble();

        System.out.println("-Segunda ecuación-");
        System.out.println("Ingresa el valor que acompaña a X:");
        x2 = teclado.nextDouble();
        System.out.println("Ingresa el valor que acompaña a Y:");
        y2 = teclado.nextDouble();
        System.out.println("Ingresa el valor que acompaña a Z:");
        z2 = teclado.nextDouble();
        System.out.println("Ingresa el valor del resultado:");
        d2 = teclado.nextDouble();

        System.out.println("-Tercera ecuación-");
        System.out.println("Ingresa el valor que acompaña a X:");
        x3 = teclado.nextDouble();
        System.out.println("Ingresa el valor que acompaña a Y:");
        y3 = teclado.nextDouble();
        System.out.println("Ingresa el valor que acompaña a Z:");
        z3 = teclado.nextDouble();
        System.out.println("Ingresa el valor del resultado:");
        d3 = teclado.nextDouble();

        System.out.println("---------ECUACION---------");
        System.out.println("X1 = ("+x1+")x1 + ("+y1+")x2 + ("+z1+")x3 = "+d1);
        System.out.println("X2 = ("+x2+")x1 + ("+y2+")x2 + ("+z2+")x3 = "+d2);
        System.out.println("X3 = ("+x3+")x1 + ("+y3+")x2 + ("+z3+")x3 = "+d3+"\n");

        System.out.println("Ingresa el porcentaje de error: ");
        porcE = teclado.nextDouble();

        do {
            System.out.println("Iteracion: " + (i + 1));
            fx1 = ( ((d1) - (y1)*(fx2) - ((z1)*(fx3)))/(x1));
            System.out.println("X1 = " + fx1);
            fx2 = (((d2) - ((x2) * (fx1)) - ((z2)*(fx3))) / (y2));
            System.out.println("X2 = " + fx2);
            fx3 = (((d3) - ((x3) * (fx1)) - ((y3) * (fx2))) / (z3));
            System.out.println("X3 = " + fx3);

            eR = Math.abs(((d1) - (((x1) * (fx1)) + ((y1) * (fx2)) + ((z1) * (fx3)))) / (d1)) * 100;
            System.out.println("Error 1: " + eR);
            eR2 = Math.abs(((d2) - (((x2) * (fx1)) + ((y2) * (fx2)) + ((z2) * (fx3)))) / (d2)) * 100;
            System.out.println("Error 2: " + eR2);
            eR3 = Math.abs(((d3) - (((x3) * (fx1)) + ((y3) * (fx2)) + ((z3) * (fx3)))) / (d3)) * 100;
            System.out.println("Error 3: " + eR3);
            i++;
            System.out.println("\n");

        }while((eR>=porcE) || (eR2>=porcE) || (eR3>=porcE));
    }

}
