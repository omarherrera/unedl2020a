import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class metodoVisual {

    public metodoVisual() {

        JFrame pnlMetodo = new JFrame("Metodo GaussSeidel");
        pnlMetodo.setSize(670, 450);
        pnlMetodo.setVisible(true);
        pnlMetodo.setLayout(null);
        pnlMetodo.setDefaultCloseOperation(pnlMetodo.EXIT_ON_CLOSE);
        boolean band = false;

        //X1
        JLabel lblX1 = new JLabel("X1: ");
        lblX1.setBounds(20, 20, 50, 30);
        pnlMetodo.add(lblX1);

        JTextField txtX1 = new JTextField();
        txtX1.setBounds(50, 20, 100, 30);
        pnlMetodo.add(txtX1);

        //X2
        JLabel lblX2 = new JLabel("X2: ");
        lblX2.setBounds(20, 60, 50, 30);
        pnlMetodo.add(lblX2);

        JTextField txtX2 = new JTextField();
        txtX2.setBounds(50, 60, 100, 30);
        pnlMetodo.add(txtX2);

        //X3
        JLabel lblX3 = new JLabel("X3: ");
        lblX3.setBounds(20, 100, 50, 30);
        pnlMetodo.add(lblX3);

        JTextField txtX3 = new JTextField();
        txtX3.setBounds(50, 100, 100, 30);
        pnlMetodo.add(txtX3);

        //Y1
        JLabel lblY1 = new JLabel("Y1: ");
        lblY1.setBounds(180, 20, 50, 30);
        pnlMetodo.add(lblY1);

        JTextField txtY1 = new JTextField();
        txtY1.setBounds(210, 20, 100, 30);
        pnlMetodo.add(txtY1);

        //Y2
        JLabel lblY2 = new JLabel("Y2: ");
        lblY2.setBounds(180, 60, 50, 30);
        pnlMetodo.add(lblY2);

        JTextField txtY2 = new JTextField();
        txtY2.setBounds(210, 60, 100, 30);
        pnlMetodo.add(txtY2);

        //Y3
        JLabel lblY3 = new JLabel("X1: ");
        lblY3.setBounds(180, 100, 50, 30);
        pnlMetodo.add(lblY3);

        JTextField txtY3 = new JTextField();
        txtY3.setBounds(210, 100, 100, 30);
        pnlMetodo.add(txtY3);

        //Z1
        JLabel lblZ1 = new JLabel("Z1: ");
        lblZ1.setBounds(340, 20, 50, 30);
        pnlMetodo.add(lblZ1);

        JTextField txtZ1 = new JTextField();
        txtZ1.setBounds(360, 20, 100, 30);
        pnlMetodo.add(txtZ1);

        //Z2
        JLabel lblZ2 = new JLabel("Z2: ");
        lblZ2.setBounds(340, 60, 50, 30);
        pnlMetodo.add(lblZ2);

        JTextField txtZ2 = new JTextField();
        txtZ2.setBounds(360, 60, 100, 30);
        pnlMetodo.add(txtZ2);

        //Z3
        JLabel lblZ3 = new JLabel("Z3: ");
        lblZ3.setBounds(340, 100, 50, 30);
        pnlMetodo.add(lblZ3);

        JTextField txtZ3 = new JTextField();
        txtZ3.setBounds(360, 100, 100, 30);
        pnlMetodo.add(txtZ3);

        //D1
        JLabel lblD1 = new JLabel("D1: ");
        lblD1.setBounds(490, 20, 50, 30);
        pnlMetodo.add(lblD1);

        JTextField txtD1 = new JTextField();
        txtD1.setBounds(510, 20, 100, 30);
        pnlMetodo.add(txtD1);

        //D2
        JLabel lblD2 = new JLabel("D2: ");
        lblD2.setBounds(490, 60, 50, 30);
        pnlMetodo.add(lblD2);

        JTextField txtD2 = new JTextField();
        txtD2.setBounds(510, 60, 100, 30);
        pnlMetodo.add(txtD2);

        //D3
        JLabel lblD3 = new JLabel("D3: ");
        lblD3.setBounds(490, 100, 50, 30);
        pnlMetodo.add(lblD3);

        JTextField txtD3 = new JTextField();
        txtD3.setBounds(510, 100, 100, 30);
        pnlMetodo.add(txtD3);

        //Extras
        JLabel lblError = new JLabel("Porc. Error: ");
        lblError.setBounds(20, 150, 100, 30);
        pnlMetodo.add(lblError);

        JTextField txtError = new JTextField();
        txtError.setBounds(90, 150, 100, 30);
        pnlMetodo.add(txtError);

        JLabel lblDec = new JLabel("Decimales: ");
        lblDec.setBounds(200, 150, 100, 30);
        pnlMetodo.add(lblDec);

        JTextField txtDec = new JTextField();
        txtDec.setBounds(270, 150, 100, 30);
        pnlMetodo.add(txtDec);

        //Botones
        JButton btnCalcular = new JButton("Calcular");
        btnCalcular.setBounds(400, 150, 100, 30);
        pnlMetodo.add(btnCalcular);

        JButton btnLimpiar = new JButton("Limpiar");
        btnLimpiar.setBounds(510, 150, 100, 30);
        pnlMetodo.add(btnLimpiar);

        //Area
        JTextArea txaShow = new JTextArea();
        txaShow.setBounds(50, 200, 530,180);
        pnlMetodo.add(txaShow);

        JScrollPane scroll = new JScrollPane(txaShow);
        scroll.setBounds(50, 200, 530, 180);
        pnlMetodo.add(scroll);

        //Funciones
        KeyListener numeros = new KeyListener() {
            @Override
            public void keyTyped(KeyEvent k) {
                char car = k.getKeyChar();
                if (!(car>='0' && car<='9')) {
                    if (!(car == '.' )) {
                        if (!(car == '-' )) {
                            k.consume();
                            //JOptionPane.showMessageDialog(null, "Ingresa solo números.");
                        }
                    }
                }
                if(car == '.' && (txtX1.getText().contains("."))) {
                    k.consume();
                }
            };


            @Override
            public void keyPressed(KeyEvent keyEvent) {

            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {

            }
        };
        txtX1.addKeyListener(numeros);

        ActionListener actCalcular = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!band){
                    String a1 = txtX1.getText().trim();
                    String b1 = txtY1.getText().trim();
                    String c1 = txtZ1.getText().trim();
                    String r1 = txtD1.getText().trim();
                    if(a1.isEmpty() || b1.isEmpty() || c1.isEmpty() || r1.isEmpty()){
                        JOptionPane.showMessageDialog(null, "No debe haber campos vacíos en la primera fila.");
                    }else{
                        String a2 = txtX2.getText().trim();
                        String b2 = txtY2.getText().trim();
                        String c2 = txtZ2.getText().trim();
                        String r2 = txtD2.getText().trim();
                        if(a2.isEmpty() || b2.isEmpty() || c2.isEmpty() || r2.isEmpty()){
                            JOptionPane.showMessageDialog(null, "No debe haber campos vacíos en la segunda fila.");
                        }else{
                            String a3 = txtX3.getText().trim();
                            String b3 = txtY3.getText().trim();
                            String c3 = txtZ3.getText().trim();
                            String r3 = txtD3.getText().trim();
                            if(a3.isEmpty() || b3.isEmpty() || c3.isEmpty() || r3.isEmpty()){
                                JOptionPane.showMessageDialog(null, "No debe haber campos vacíos en la tecera fila.");
                            }else{
                                String porcError = txtError.getText().trim();
                                String numD = txtDec.getText().trim();
                                if(porcError.isEmpty() || numD.isEmpty()){
                                    JOptionPane.showMessageDialog(null, "Los campos de error y decimales no pueden estar vacíos.");
                                }else{
                                    double x1 = Double.parseDouble(a1);
                                    double x2 = Double.parseDouble(a2);
                                    double x3 = Double.parseDouble(a3);
                                    double y1 = Double.parseDouble(b1);
                                    double y2 = Double.parseDouble(b2);
                                    double y3 = Double.parseDouble(b3);
                                    double z1 = Double.parseDouble(c1);
                                    double z2 = Double.parseDouble(c2);
                                    double z3 = Double.parseDouble(c3);
                                    double d1 = Double.parseDouble(r1);
                                    double d2 = Double.parseDouble(r2);
                                    double d3 = Double.parseDouble(r3);
                                    double porcE = Double.parseDouble(porcError);
                                    int dec = Integer.parseInt(numD);
                                    double eR, eR2, eR3;
                                    double fx1 = 0, fx2 = 0, fx3 = 0;
                                    int i = 0;
                                    switch (dec) {
                                        case 1:
                                            do {
                                                txaShow.append(String.format("Iteracion: %d\n", (i + 1)));
                                                fx1 = (((d1) - (y1) * (fx2) - ((z1) * (fx3))) / (x1));
                                                txaShow.append(String.format("X1 = %.1f\n", fx1));
                                                fx2 = (((d2) - ((x2) * (fx1)) - ((z2) * (fx3))) / (y2));
                                                txaShow.append(String.format("X2 = %.1f\n", fx2));
                                                fx3 = (((d3) - ((x3) * (fx1)) - ((y3) * (fx2))) / (z3));
                                                txaShow.append(String.format("X3 = %.1f\n", fx3));

                                                eR = Math.abs(((d1) - (((x1) * (fx1)) + ((y1) * (fx2)) + ((z1) * (fx3)))) / (d1)) * 100;
                                                txaShow.append(String.format("Error 1: %.1f\n", eR));
                                                eR2 = Math.abs(((d2) - (((x2) * (fx1)) + ((y2) * (fx2)) + ((z2) * (fx3)))) / (d2)) * 100;
                                                txaShow.append(String.format("Error 2: %.1f\n", eR2));
                                                eR3 = Math.abs(((d3) - (((x3) * (fx1)) + ((y3) * (fx2)) + ((z3) * (fx3)))) / (d3)) * 100;
                                                txaShow.append(String.format("Error 3: %.1f\n\n", eR3));
                                                i++;
                                            } while ((eR >= porcE) || (eR2 >= porcE) || (eR3 >= porcE));
                                            break;
                                        case 2:
                                            do {
                                                txaShow.append(String.format("Iteracion: %d\n", (i + 1)));
                                                fx1 = (((d1) - (y1) * (fx2) - ((z1) * (fx3))) / (x1));
                                                txaShow.append(String.format("X1 = %.2f\n", fx1));
                                                fx2 = (((d2) - ((x2) * (fx1)) - ((z2) * (fx3))) / (y2));
                                                txaShow.append(String.format("X2 = %.2f\n", fx2));
                                                fx3 = (((d3) - ((x3) * (fx1)) - ((y3) * (fx2))) / (z3));
                                                txaShow.append(String.format("X3 = %.2f\n", fx3));

                                                eR = Math.abs(((d1) - (((x1) * (fx1)) + ((y1) * (fx2)) + ((z1) * (fx3)))) / (d1)) * 100;
                                                txaShow.append(String.format("Error 1: %.2f\n", eR));
                                                eR2 = Math.abs(((d2) - (((x2) * (fx1)) + ((y2) * (fx2)) + ((z2) * (fx3)))) / (d2)) * 100;
                                                txaShow.append(String.format("Error 2: %.2f\n", eR2));
                                                eR3 = Math.abs(((d3) - (((x3) * (fx1)) + ((y3) * (fx2)) + ((z3) * (fx3)))) / (d3)) * 100;
                                                txaShow.append(String.format("Error 3: %.2f\n\n", eR3));
                                                i++;
                                            } while ((eR >= porcE) || (eR2 >= porcE) || (eR3 >= porcE));
                                            break;
                                        case 3:
                                            do {
                                                txaShow.append(String.format("Iteracion: %d\n", (i + 1)));
                                                fx1 = (((d1) - (y1) * (fx2) - ((z1) * (fx3))) / (x1));
                                                txaShow.append(String.format("X1 = %.3f\n", fx1));
                                                fx2 = (((d2) - ((x2) * (fx1)) - ((z2) * (fx3))) / (y2));
                                                txaShow.append(String.format("X2 = %.3f\n", fx2));
                                                fx3 = (((d3) - ((x3) * (fx1)) - ((y3) * (fx2))) / (z3));
                                                txaShow.append(String.format("X3 = %.3f\n", fx3));

                                                eR = Math.abs(((d1) - (((x1) * (fx1)) + ((y1) * (fx2)) + ((z1) * (fx3)))) / (d1)) * 100;
                                                txaShow.append(String.format("Error 1: %.3f\n", eR));
                                                eR2 = Math.abs(((d2) - (((x2) * (fx1)) + ((y2) * (fx2)) + ((z2) * (fx3)))) / (d2)) * 100;
                                                txaShow.append(String.format("Error 2: %.3f\n", eR2));
                                                eR3 = Math.abs(((d3) - (((x3) * (fx1)) + ((y3) * (fx2)) + ((z3) * (fx3)))) / (d3)) * 100;
                                                txaShow.append(String.format("Error 3: %.3f\n\n", eR3));
                                                i++;
                                            } while ((eR >= porcE) || (eR2 >= porcE) || (eR3 >= porcE));
                                            break;
                                        case 4:
                                            do {
                                                txaShow.append(String.format("Iteracion: %d\n", (i + 1)));
                                                fx1 = (((d1) - (y1) * (fx2) - ((z1) * (fx3))) / (x1));
                                                txaShow.append(String.format("X1 = %.4f\n", fx1));
                                                fx2 = (((d2) - ((x2) * (fx1)) - ((z2) * (fx3))) / (y2));
                                                txaShow.append(String.format("X2 = %.4f\n", fx2));
                                                fx3 = (((d3) - ((x3) * (fx1)) - ((y3) * (fx2))) / (z3));
                                                txaShow.append(String.format("X3 = %.4f\n", fx3));

                                                eR = Math.abs(((d1) - (((x1) * (fx1)) + ((y1) * (fx2)) + ((z1) * (fx3)))) / (d1)) * 100;
                                                txaShow.append(String.format("Error 1: %.4f\n", eR));
                                                eR2 = Math.abs(((d2) - (((x2) * (fx1)) + ((y2) * (fx2)) + ((z2) * (fx3)))) / (d2)) * 100;
                                                txaShow.append(String.format("Error 2: %.4f\n", eR2));
                                                eR3 = Math.abs(((d3) - (((x3) * (fx1)) + ((y3) * (fx2)) + ((z3) * (fx3)))) / (d3)) * 100;
                                                txaShow.append(String.format("Error 3: %.4f\n\n", eR3));
                                                i++;
                                            } while ((eR >= porcE) || (eR2 >= porcE) || (eR3 >= porcE));
                                            break;
                                        case 5:
                                            do {
                                                txaShow.append(String.format("Iteracion: %d\n", (i + 1)));
                                                fx1 = (((d1) - (y1) * (fx2) - ((z1) * (fx3))) / (x1));
                                                txaShow.append(String.format("X1 = %.5f\n", fx1));
                                                fx2 = (((d2) - ((x2) * (fx1)) - ((z2) * (fx3))) / (y2));
                                                txaShow.append(String.format("X2 = %.5f\n", fx2));
                                                fx3 = (((d3) - ((x3) * (fx1)) - ((y3) * (fx2))) / (z3));
                                                txaShow.append(String.format("X3 = %.5f\n", fx3));

                                                eR = Math.abs(((d1) - (((x1) * (fx1)) + ((y1) * (fx2)) + ((z1) * (fx3)))) / (d1)) * 100;
                                                txaShow.append(String.format("Error 1: %.5f\n", eR));
                                                eR2 = Math.abs(((d2) - (((x2) * (fx1)) + ((y2) * (fx2)) + ((z2) * (fx3)))) / (d2)) * 100;
                                                txaShow.append(String.format("Error 2: %.5f\n", eR2));
                                                eR3 = Math.abs(((d3) - (((x3) * (fx1)) + ((y3) * (fx2)) + ((z3) * (fx3)))) / (d3)) * 100;
                                                txaShow.append(String.format("Error 3: %.5f\n\n", eR3));
                                                i++;
                                            } while ((eR >= porcE) || (eR2 >= porcE) || (eR3 >= porcE));
                                            break;
                                        case 6:
                                            do {
                                                txaShow.append(String.format("Iteracion: %d\n", (i + 1)));
                                                fx1 = (((d1) - (y1) * (fx2) - ((z1) * (fx3))) / (x1));
                                                txaShow.append(String.format("X1 = %.6f\n", fx1));
                                                fx2 = (((d2) - ((x2) * (fx1)) - ((z2) * (fx3))) / (y2));
                                                txaShow.append(String.format("X2 = %.6f\n", fx2));
                                                fx3 = (((d3) - ((x3) * (fx1)) - ((y3) * (fx2))) / (z3));
                                                txaShow.append(String.format("X3 = %.6f\n", fx3));

                                                eR = Math.abs(((d1) - (((x1) * (fx1)) + ((y1) * (fx2)) + ((z1) * (fx3)))) / (d1)) * 100;
                                                txaShow.append(String.format("Error 1: %.6f\n", eR));
                                                eR2 = Math.abs(((d2) - (((x2) * (fx1)) + ((y2) * (fx2)) + ((z2) * (fx3)))) / (d2)) * 100;
                                                txaShow.append(String.format("Error 2: %.6f\n", eR2));
                                                eR3 = Math.abs(((d3) - (((x3) * (fx1)) + ((y3) * (fx2)) + ((z3) * (fx3)))) / (d3)) * 100;
                                                txaShow.append(String.format("Error 3: %.6f\n\n", eR3));
                                                i++;
                                            } while ((eR >= porcE) || (eR2 >= porcE) || (eR3 >= porcE));
                                            break;
                                        case 7:
                                            do {
                                                txaShow.append(String.format("Iteracion: %d\n", (i + 1)));
                                                fx1 = (((d1) - (y1) * (fx2) - ((z1) * (fx3))) / (x1));
                                                txaShow.append(String.format("X1 = %.7f\n", fx1));
                                                fx2 = (((d2) - ((x2) * (fx1)) - ((z2) * (fx3))) / (y2));
                                                txaShow.append(String.format("X2 = %.7f\n", fx2));
                                                fx3 = (((d3) - ((x3) * (fx1)) - ((y3) * (fx2))) / (z3));
                                                txaShow.append(String.format("X3 = %.7f\n", fx3));

                                                eR = Math.abs(((d1) - (((x1) * (fx1)) + ((y1) * (fx2)) + ((z1) * (fx3)))) / (d1)) * 100;
                                                txaShow.append(String.format("Error 1: %.7f\n", eR));
                                                eR2 = Math.abs(((d2) - (((x2) * (fx1)) + ((y2) * (fx2)) + ((z2) * (fx3)))) / (d2)) * 100;
                                                txaShow.append(String.format("Error 2: %.7f\n", eR2));
                                                eR3 = Math.abs(((d3) - (((x3) * (fx1)) + ((y3) * (fx2)) + ((z3) * (fx3)))) / (d3)) * 100;
                                                txaShow.append(String.format("Error 3: %.7f\n\n", eR3));
                                                i++;
                                            } while ((eR >= porcE) || (eR2 >= porcE) || (eR3 >= porcE));
                                            break;
                                        case 8:
                                            do {
                                                txaShow.append(String.format("Iteracion: %d\n", (i + 1)));
                                                fx1 = (((d1) - (y1) * (fx2) - ((z1) * (fx3))) / (x1));
                                                txaShow.append(String.format("X1 = %.8f\n", fx1));
                                                fx2 = (((d2) - ((x2) * (fx1)) - ((z2) * (fx3))) / (y2));
                                                txaShow.append(String.format("X2 = %.8f\n", fx2));
                                                fx3 = (((d3) - ((x3) * (fx1)) - ((y3) * (fx2))) / (z3));
                                                txaShow.append(String.format("X3 = %.8f\n", fx3));

                                                eR = Math.abs(((d1) - (((x1) * (fx1)) + ((y1) * (fx2)) + ((z1) * (fx3)))) / (d1)) * 100;
                                                txaShow.append(String.format("Error 1: %.8f\n", eR));
                                                eR2 = Math.abs(((d2) - (((x2) * (fx1)) + ((y2) * (fx2)) + ((z2) * (fx3)))) / (d2)) * 100;
                                                txaShow.append(String.format("Error 2: %.8f\n", eR2));
                                                eR3 = Math.abs(((d3) - (((x3) * (fx1)) + ((y3) * (fx2)) + ((z3) * (fx3)))) / (d3)) * 100;
                                                txaShow.append(String.format("Error 3: %.8f\n\n", eR3));
                                                i++;
                                            } while ((eR >= porcE) || (eR2 >= porcE) || (eR3 >= porcE));
                                            break;
                                        case 9:
                                            do {
                                                txaShow.append(String.format("Iteracion: %d\n", (i + 1)));
                                                fx1 = (((d1) - (y1) * (fx2) - ((z1) * (fx3))) / (x1));
                                                txaShow.append(String.format("X1 = %.9f\n", fx1));
                                                fx2 = (((d2) - ((x2) * (fx1)) - ((z2) * (fx3))) / (y2));
                                                txaShow.append(String.format("X2 = %.9f\n", fx2));
                                                fx3 = (((d3) - ((x3) * (fx1)) - ((y3) * (fx2))) / (z3));
                                                txaShow.append(String.format("X3 = %.9f\n", fx3));

                                                eR = Math.abs(((d1) - (((x1) * (fx1)) + ((y1) * (fx2)) + ((z1) * (fx3)))) / (d1)) * 100;
                                                txaShow.append(String.format("Error 1: %.9f\n", eR));
                                                eR2 = Math.abs(((d2) - (((x2) * (fx1)) + ((y2) * (fx2)) + ((z2) * (fx3)))) / (d2)) * 100;
                                                txaShow.append(String.format("Error 2: %.9f\n", eR2));
                                                eR3 = Math.abs(((d3) - (((x3) * (fx1)) + ((y3) * (fx2)) + ((z3) * (fx3)))) / (d3)) * 100;
                                                txaShow.append(String.format("Error 3: %.9f\n\n", eR3));
                                                i++;
                                            } while ((eR >= porcE) || (eR2 >= porcE) || (eR3 >= porcE));
                                            break;
                                        default:
                                            JOptionPane.showMessageDialog(null, "Ingresa una opcion valida.");
                                    }
                                }
                            }
                        }
                    }
                }else{
                    JOptionPane.showMessageDialog(null, "Primero limpia la pantalla.");
                }
            }
        };
        btnCalcular.addActionListener(actCalcular);

    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new metodoVisual();
            }
        });
    }

}
