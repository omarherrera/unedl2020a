import java.util.*;

public class Principal {

    public static void main(String args[]) {
        Scanner c = new Scanner(System.in);
        int Decimal = 0;
        Ecuacion e = new Ecuacion();


        System.out.println("ingrese los elementos que tendra la ecuacion: ");
        e.Iniciar(c.nextInt());

        for (int i = 0; i < e.Funcion.length; i++) {

            System.out.println("Constante o variable?\nEscribe C o V");

            String primeraFase = c.next();
            if (primeraFase.equals("C") || primeraFase.equals("c")) {
                System.out.println ("La constante es positiva o negativa?\nEscribe P o N");

                String constanteFaseUno = c. next();

                boolean flagUno = true;
                boolean operacion = false;
                do {
                    if (constanteFaseUno.equals("P") || constanteFaseUno.equals("p")) {
                        operacion = true;
                    } else if (constanteFaseUno.equals("N") || constanteFaseUno.equals("n")) {
                        operacion = false;
                    } else {
                        System.out.println("No puedes colocar un digito o caracter distinto");

                        constanteFaseUno = "";
                        flagUno = false;
                    }
                } while (flagUno != true);


                System.out.println ("Ingrese el numero, solo valores positivos:");

                String constanteFaseDos = c.next();


                e.Funcion[i] = 1;

                if (operacion) {
                    e.Producto[i] = Double.parseDouble(constanteFaseDos);
                } else {
                    String aux2 = "-";

                    String aux = aux2 + constanteFaseDos;


                    e.Producto[i] = Double.parseDouble(aux);


                }

                e.Potencia[i] = 1;


            } else if (primeraFase.equals("V") || primeraFase.equals("v")) {


                System.out.println("Tiene alguna constante para multiplicar por la variable?");

                double variableFaseDos = c.nextDouble();

                System.out.println("La Variable es positiva o negativa?\nEscribe P o N");


                String variableFaseUno = c.next();
                boolean flagUno = true;
                boolean operacion = false;

                do {
                    if (variableFaseUno.equals("P") || variableFaseUno.equals("p")) {
                        operacion = true;
                    } else if (variableFaseUno.equals("N") || variableFaseUno.equals("n")) {
                        operacion = false;
                    } else {
                        System.out.println("No puedes colocar un digito o caracter distinto");

                        variableFaseUno = "";
                        flagUno = false;
                    }
                } while (flagUno != true);
                System.out.println("Ingrese el exponente de la variable:");
                double variableFaseTres = c.nextDouble();
                e.Potencia[i] = variableFaseTres;


                if (operacion) {
                    e.Producto[i] = variableFaseDos;
                } else {
                    String aux2 = "-";

                    String aux = aux2 + variableFaseDos;

                    variableFaseDos = Double.parseDouble(aux);

                    e.Producto[i] = variableFaseDos;
                }

                e.Funcion[i] = 100;

            } else {
                System.out.println("No puedes colocar un digito o caracter distinto");
                primeraFase = "";
                i--;
            }


        }


        //hasta este punto ya tenemos la funcion
        //lo siguiente es mostrarla en pantalla

        System.out.println("esta ha sido la funcion");
        System.out.println("" + e.mostrarFuncion());
        e.funcionPolinomica();

        //hasta aqui pedimos si el usuario quiere introducir los invervalos validos, o que el sistema los encuentre.
        System.out.println("\nPresione 0 para introducir los intervalos.\nPresione 1 para que el sistema encuentre los intervalos.\nIngrese opcion: ");
        String op = c.next();

        double intervaloA = 0;
        double intervaloB = 0;
        double resultadoA = 0;
        double resultadoB = 0;
        boolean tem = false;
        boolean flag = false;
        double puntoMedio = 0;

        //aqui hacemos el switch que nos determinara los intervalos
        do {


            switch (op) {
                case "0":
                    do {

                        System.out.println("ingrese intervalo A: ");
                        intervaloA = c.nextDouble();
                        System.out.println("ingrese intervalo B: ");
                        intervaloB = c.nextDouble();

                        tem = e.intervaloUsuario(intervaloA, intervaloB);
                        if (tem) {
                            System.out.println("El resultado es bueno y podemos iterar sobre estos valores\n");
                            op = "0";
                            puntoMedio = (intervaloA + intervaloB )/2;
                        } else {
                            System.out.println("El resultado es malo, no se pueden trabajar estos valores");
                            System.out.println("Desea seguir escogiendo introducir los datos?" +
                                    "\nPresione 0 para introducir nuevos intervalos\n" +
                                    "Presione 1 para dejar que el programa busque los intervalo\n" +
                                    "Escoja una opcion: ");
                            op = c.next();
                            if(op.equals("1")){
                                tem = true;
                            }
                        }


                    } while (tem != true);

                    break;
                case "1":
                    System.out.println("LLegamos con bien hasta aqui");

                    boolean res = false;//e.intervaloAutomatico();
                    if(res)
                    {
                        System.out.println("Se encontraron los intervalos para iterar:\n" +
                                "Intervalo A: " + e.iA +"" +
                                "\nIntervalo B: " + e.iB );
                    }else
                    {
                        System.out.println("muy probablemente la funcion sea con raices imaginarias :3");

                    }

                    break;

            }

        }while(tem!=true);

        double Xn = 0;
        double Xa = 0;
        double error = 0.05;
        double errorRelativo = 100;
        double x0 = puntoMedio;
        double Iteracion  = 0 ;
        int item = 1;

        //hacemos el procedimiento de horner

        do {


            Iteracion = x0 - (e.HornerR(x0) / e.HornerAsterisco(x0));

            Iteracion = e.redondeo(Iteracion, 4);
            Xn = Iteracion;
            Xa = x0;

            System.out.println("-----------------------------------------------Iteracion " + item + "\nla raiz vale : "+ Iteracion);
            errorRelativo = Math.abs((Xn - Xa) / Xn) * 100;
            System.out.println(Xn + " - " + Xa + "/"+ Xn );
            errorRelativo = e.redondeo(errorRelativo, 5);
            System.out.println("el error relativo es : " + errorRelativo + "%\n");

            x0 = Iteracion;
            item++;
        }while(errorRelativo>error);

        System.out.println("\nla raiz es : " + Iteracion);
    }
}
