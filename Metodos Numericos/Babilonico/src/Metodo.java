import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Metodo{

    private JButton btnRespuesta, btnLimpiar;
    private JTextField txtRaiz, txtDec, txtPorc, txtNum;
    private JLabel lblRaiz, lblDec, lblPorc, lblNum, lblNum2;
    private JTextArea txaRes;
    private KeyListener numeros, numeros2, numeros3, numeros4;
    private ActionListener actRes, actLimpiar;
    boolean bandera = false;

    public Metodo() {

        JFrame pnlMetodo = new JFrame("Metodo Babilonico");
        pnlMetodo.setSize(400, 400);
        pnlMetodo.setVisible(true);
        pnlMetodo.setLayout(null);
        pnlMetodo.setDefaultCloseOperation(pnlMetodo.EXIT_ON_CLOSE);

        lblRaiz = new JLabel("Numero a obtener su raiz: ");
        lblRaiz.setBounds(20, 20, 150, 30);
        pnlMetodo.add(lblRaiz);

        txtRaiz = new JTextField();
        txtRaiz.setBounds(200, 20, 150, 30);
        pnlMetodo.add(txtRaiz);

        lblNum = new JLabel("Numero al cuadrado proximo");
        lblNum.setBounds(20, 60, 350, 30);
        pnlMetodo.add(lblNum);

        lblNum2 = new JLabel("al numero a calcular la raiz: ");
        lblNum2.setBounds(20, 75, 350, 30);
        pnlMetodo.add(lblNum2);

        txtNum = new JTextField();
        txtNum.setBounds(200, 67, 150, 30);
        pnlMetodo.add(txtNum);

        lblPorc = new JLabel("Porcentaje de error: ");
        lblPorc.setBounds(20, 110, 150, 30);
        pnlMetodo.add(lblPorc);

        txtPorc = new JTextField();
        txtPorc.setBounds(200, 110, 150, 30);
        pnlMetodo.add(txtPorc);

        lblDec = new JLabel("Decimales (0-9): ");
        lblDec.setBounds(20, 150, 150, 30);
        pnlMetodo.add(lblDec);

        txtDec = new JTextField();
        txtDec.setBounds(200, 150, 150, 30);
        pnlMetodo.add(txtDec);

        btnRespuesta = new JButton("Calcular");
        btnRespuesta.setBounds(190, 190, 100, 30);
        pnlMetodo.add(btnRespuesta);

        btnLimpiar = new JButton("Limpiar");
        btnLimpiar.setBounds(80, 190, 100, 30);
        pnlMetodo.add(btnLimpiar);

        txaRes = new JTextArea();
        txaRes.setBounds(20, 230, 345, 90);
        pnlMetodo.add(txaRes);

        JScrollPane scroll = new JScrollPane(txaRes);
        scroll.setBounds(20, 230, 345, 90);
        pnlMetodo.add(scroll);

        numeros = new KeyListener() {
            @Override
            public void keyTyped(KeyEvent k) {
                char car = k.getKeyChar();
                if (!(car>='0' && car<='9')) {
                    if (!(car == '.')) {
                        k.consume();
                        JOptionPane.showMessageDialog(null, "Ingresa solo números.");
                    }
                }
                if(car == '.' && (txtRaiz.getText().contains("."))){
                    k.consume();
                }
            }


            @Override
            public void keyPressed(KeyEvent keyEvent) {

            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {

            }
        };
        txtRaiz.addKeyListener(numeros);

        numeros2 = new KeyListener() {
            @Override
            public void keyTyped(KeyEvent k) {
                char car = k.getKeyChar();
                if (!(car>='0' && car<='9')) {
                    /*if (!(car == '.')) {*/
                    k.consume();
                    //JOptionPane.showMessageDialog(null, "Ingresa solo números.");
                    //}
                }
                /*if(car == '.' && (txtNum.getText().contains("."))){
                    k.consume();
                }*/
            }


            @Override
            public void keyPressed(KeyEvent keyEvent) {

            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {

            }
        };
        txtNum.addKeyListener(numeros2);

        numeros3 = new KeyListener() {
            @Override
            public void keyTyped(KeyEvent k) {
                char car = k.getKeyChar();
                if (!(car>='0' && car<='9')) {
                    if (!(car == '.')) {
                        k.consume();
                        //JOptionPane.showMessageDialog(null, "Ingresa solo números.");
                    }
                }
                if(car == '.' && (txtPorc.getText().contains("."))){
                    k.consume();
                }
            }


            @Override
            public void keyPressed(KeyEvent keyEvent) {

            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {

            }
        };
        txtPorc.addKeyListener(numeros3);

        numeros4 = new KeyListener() {
            @Override
            public void keyTyped(KeyEvent k) {
                char car = k.getKeyChar();
                if (!(car>='0' && car<='9')) {
                    /*if (!(car == '.')) {*/
                        k.consume();
                        //JOptionPane.showMessageDialog(null, "Ingresa solo números.");
                    //}
                }
                /*if(car == '.' && (txtNum.getText().contains("."))){
                    k.consume();
                }*/
            }


            @Override
            public void keyPressed(KeyEvent keyEvent) {

            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {

            }
        };
        txtDec.addKeyListener(numeros4);

        actRes = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (bandera != true) {

                    String r = txtRaiz.getText().trim();
                    if (r.isEmpty()) {
                        JOptionPane.showMessageDialog(null, "El campo 1 no puede estar vacio.");
                    } else {
                        String n = txtNum.getText().trim();
                        if (n.isEmpty()) {
                            JOptionPane.showMessageDialog(null, "El campo 2 no puede estar vacio.");
                        } else {
                            String p = txtPorc.getText().trim();
                            if (p.isEmpty()) {
                                JOptionPane.showMessageDialog(null, "El campo 3 no puede estar vacio.");
                            } else {
                                String d = txtDec.getText().trim();
                                if (d.isEmpty()) {
                                    JOptionPane.showMessageDialog(null, "El campo 4 no puede estar vacio.");
                                } else {
                                    double c = Double.parseDouble(r);
                                    double x0 = Double.parseDouble(n);
                                    double e = Double.parseDouble(p);
                                    int dec = Integer.parseInt(d);
                                    if (c > 0) {
                                        if (x0 <= c) {
                                            if (e <= 100) {
                                                if (dec >= 1 && dec <= 9) {
                                                    double X = 0;
                                                    double eR = 0;
                                                    txaRes.append(String.valueOf(Math.sqrt(c)));
                                                    bandera = true;
                                                    if (dec == 1) {
                                                        do {
                                                            X = 0.5 * (x0 + (c / x0));
                                                            eR = Math.abs((X - x0) / X) * 100;
                                                            //System.out.println(String.format("Aproximacion: %.1f Error Relativo: %f", X, eR));
                                                            txaRes.append(String.format("\nAproximacion: %.1f Error Relativo: %f", X, eR));
                                                            x0 = X;
                                                        } while ((eR >= e));
                                                    } else if (dec == 2) {
                                                        do {
                                                            X = 0.5 * (x0 + (c / x0));
                                                            eR = Math.abs((X - x0) / X) * 100;
                                                            //System.out.println(String.format("Aproximacion: %.2f Error Relativo: %f", X, eR));
                                                            txaRes.append(String.format("\nAproximacion: %.2f Error Relativo: %f", X, eR));
                                                            x0 = X;
                                                        } while (eR >= e);
                                                    } else if (dec == 3) {
                                                        do {
                                                            X = 0.5 * (x0 + (c / x0));
                                                            eR = Math.abs((X - x0) / X) * 100;
                                                            //System.out.println(String.format("Aproximacion: %.3f Error Relativo: %f", X, eR));
                                                            txaRes.append(String.format("\nAproximacion: %.3f Error Relativo: %f", X, eR));
                                                            x0 = X;
                                                        } while (eR >= e);
                                                    } else if (dec == 4) {
                                                        do {
                                                            X = 0.5 * (x0 + (c / x0));
                                                            eR = Math.abs((X - x0) / X) * 100;
                                                            //System.out.println(String.format("Aproximacion: %.4f Error Relativo: %f", X, eR));
                                                            txaRes.append(String.format("\nAproximacion: %.4f Error Relativo: %f", X, eR));
                                                            x0 = X;
                                                        } while (eR >= e);
                                                    } else if (dec == 5) {
                                                        do {
                                                            X = 0.5 * (x0 + (c / x0));
                                                            eR = Math.abs((X - x0) / X) * 100;
                                                            //System.out.println(String.format("Aproximacion: %.5f Error Relativo: %f", X, eR));
                                                            txaRes.append(String.format("\nAproximacion: %.5f Error Relativo: %f", X, eR));
                                                            x0 = X;
                                                        } while (eR >= e);
                                                    } else if (dec == 6) {
                                                        do {
                                                            X = 0.5 * (x0 + (c / x0));
                                                            eR = Math.abs((X - x0) / X) * 100;
                                                            //System.out.println(String.format("Aproximacion: %.6f Error Relativo: %f", X, eR));
                                                            txaRes.append(String.format("\nAproximacion: %.6f Error Relativo: %f", X, eR));
                                                            x0 = X;
                                                        } while (eR >= e);
                                                    } else if (dec == 7) {
                                                        do {
                                                            X = 0.5 * (x0 + (c / x0));
                                                            eR = Math.abs((X - x0) / X) * 100;
                                                            //System.out.println(String.format("Aproximacion: %.7f Error Relativo: %f", X, eR));
                                                            txaRes.append(String.format("\nAproximacion: %.7f Error Relativo: %f", X, eR));
                                                            x0 = X;
                                                        } while (eR >= e);
                                                    } else if (dec == 8) {
                                                        do {
                                                            X = 0.5 * (x0 + (c / x0));
                                                            eR = Math.abs((X - x0) / X) * 100;
                                                            //System.out.println(String.format("Aproximacion: %.8f Error Relativo: %f", X, eR));
                                                            txaRes.append(String.format("\nAproximacion: %.8f Error Relativo: %f", X, eR));
                                                            x0 = X;
                                                        } while (eR >= e);
                                                    } else if (dec == 9) {
                                                        do {
                                                            X = 0.5 * (x0 + (c / x0));
                                                            eR = Math.abs((X - x0) / X) * 100;
                                                            //System.out.println(String.format("Aproximacion: %.9f Error Relativo: %f", X, eR));
                                                            txaRes.append(String.format("\nAproximacion: %.9f Error Relativo: %f", X, eR));
                                                            x0 = X;
                                                        } while (eR >= e);
                                                    } else {
                                                        JOptionPane.showMessageDialog(null, "Ingresa valores correctos.");
                                                    }
                                                } else {
                                                    JOptionPane.showMessageDialog(null, "Los valores deben ser entre 1 y 9.");
                                                }
                                            } else {
                                                JOptionPane.showMessageDialog(null, "El valor del error debe ser menor o igual a 100.");
                                            }
                                        } else {
                                            JOptionPane.showMessageDialog(null, "El cuadrado no puede ser mayor que la raiz.");
                                        }
                                    } else {
                                        JOptionPane.showMessageDialog(null, "La raiz debe ser mayor a 0.");
                                    }
                                }
                            }
                        }
                    }
                }else{
                    JOptionPane.showMessageDialog(null, "Primero limpia la pantalla.");
                }
            }
        };
        btnRespuesta.addActionListener(actRes);

        actLimpiar = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                txtRaiz.setText("");
                txtNum.setText("");
                txtPorc.setText("");
                txtDec.setText("");
                txaRes.setText("");
            }
        };
        btnLimpiar.addActionListener(actLimpiar);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Metodo();
            }
        });
    }

}
