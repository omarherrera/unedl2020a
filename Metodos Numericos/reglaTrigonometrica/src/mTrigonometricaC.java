import java.util.Scanner;

public class mTrigonometricaC {

    static double f(double x) { return ( ((Math.pow(x, 3))) - ((2)*((Math.pow(x, 2)))) - ((5)*(x)) + (2) ); }

    //seno
    static double Fs(double f){return Math.sin(f);}
    //coseno
    static double Fc(double f){return Math.cos(f);}
    //tangente
    static double Ft(double f){return Math.tan(f);}
    //secante
    static double Fsc(double f){return 1/(Math.cos(f));}
    //cosecante
    static double Fcs(double f){return 1/(Math.sin(f));}
    //cotangente
    static double Fct(double f){return 1/(Math.tan(f));}

    public static void main(String[] args) {

        Scanner teclado = new Scanner(System.in);
        double a, b, e, eR, Az, Bz, ax, bx;
        double xR, xRN;
        int op, op2, i;

        System.out.println("Valores en: Radianes -> 1     Grados -> 2");
        op = teclado.nextInt();
        System.out.println(("Operacion que acompaña f(x):\n1. Seno\n2. Coseno\n3. Tangente\n4. Secante\n5. Cosecante\n6. Cotangente "));
        op2 = teclado.nextInt();
        switch(op) {
            case 1:
                switch (op2) {
                    case 1:
                        System.out.println("Ingresa el valor de a: ");
                        a = teclado.nextDouble();
                        System.out.println("Ingresa el valor de b: ");
                        b = teclado.nextDouble();
                        System.out.println("Ingresa el porcentaje de error: ");
                        e = teclado.nextDouble();

                        Az = Fs(f(a));
                        Bz = Fs(f(b));

                        xR=(Az*b - Bz*a)/ (Az-Bz);
                        i=0;

                        if(Az*Fs(f(xR))==0) {
                            System.out.printf("La raiz es: %f", b);
                        } else if(Az*Fs(f(xR))<0) {
                            Bz=xR;
                        } else {
                            do {
                                xRN = b - ((Bz * xR - Bz * b) / (Fs(f(xR)) - Bz));
                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                System.out.printf("\nIteracion:%d  XRN: %f    Error Relativo: %f", i, xRN, eR);
                                xR = xRN;
                                i++;
                            } while (eR >= e);
                        }
                        break;
                    case 2:
                        System.out.println("Ingresa el valor de a: ");
                        a = teclado.nextDouble();
                        System.out.println("Ingresa el valor de b: ");
                        b = teclado.nextDouble();
                        System.out.println("Ingresa el porcentaje de error: ");
                        e = teclado.nextDouble();

                        Az = Fc(f(a));
                        Bz = Fc(f(b));

                        xR=(Az*b - Bz*a)/ (Az-Bz);
                        i=0;

                        if(Az*Fc(f(xR))==0) {
                            System.out.printf("La raiz es: %f", b);
                        } else if(Az*Fc(f(xR))<0) {
                            Bz=xR;
                        } else {
                            do {
                                xRN = b - ((Bz * xR - Bz * b) / (Fc(f(xR)) - Bz));
                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                System.out.printf("\nIteracion:%d  XRN: %f    Error Relativo: %f", i, xRN, eR);
                                xR = xRN;
                                i++;
                            } while (eR >= e);
                        }
                        break;
                    case 3:
                        System.out.println("Ingresa el valor de a: ");
                        a = teclado.nextDouble();
                        System.out.println("Ingresa el valor de b: ");
                        b = teclado.nextDouble();
                        System.out.println("Ingresa el porcentaje de error: ");
                        e = teclado.nextDouble();

                        Az = Ft(f(a));
                        Bz = Ft(f(b));

                        xR=(Az*b - Bz*a)/ (Az-Bz);
                        i=0;

                        if(Az*Ft(f(xR))==0) {
                            System.out.printf("La raiz es: %f", b);
                        } else if(Az*Ft(f(xR))<0) {
                            Bz=xR;
                        } else {
                            do {
                                xRN = b - ((Bz * xR - Bz * b) / (Ft(f(xR)) - Bz));
                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                System.out.printf("\nIteracion:%d  XRN: %f    Error Relativo: %f", i, xRN, eR);
                                xR = xRN;
                                i++;
                            } while (eR >= e);
                        }
                        break;
                    case 4:
                        System.out.println("Ingresa el valor de a: ");
                        a = teclado.nextDouble();
                        System.out.println("Ingresa el valor de b: ");
                        b = teclado.nextDouble();
                        System.out.println("Ingresa el porcentaje de error: ");
                        e = teclado.nextDouble();

                        Az = Fsc(f(a));
                        Bz = Fsc(f(b));

                        xR=(Az*b - Bz*a)/ (Az-Bz);
                        i=0;

                        if(Az*Fsc(f(xR))==0) {
                            System.out.printf("La raiz es: %f", b);
                        } else if(Az*Fsc(f(xR))<0) {
                            Bz=xR;
                        } else {
                            do {
                                xRN = b - ((Bz * xR - Bz * b) / (Fsc(f(xR)) - Bz));
                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                System.out.printf("\nIteracion:%d  XRN: %f    Error Relativo: %f", i, xRN, eR);
                                xR = xRN;
                                i++;
                            } while (eR >= e);
                        }
                        break;
                    case 5:
                        System.out.println("Ingresa el valor de a: ");
                        a = teclado.nextDouble();
                        System.out.println("Ingresa el valor de b: ");
                        b = teclado.nextDouble();
                        System.out.println("Ingresa el porcentaje de error: ");
                        e = teclado.nextDouble();

                        Az = Fcs(f(a));
                        Bz = Fcs(f(b));

                        xR=(Az*b - Bz*a)/ (Az-Bz);
                        i=0;

                        if(Az*Fcs(f(xR))==0) {
                            System.out.printf("La raiz es: %f", b);
                        } else if(Az*Fcs(f(xR))<0) {
                            Bz=xR;
                        } else {
                            do {
                                xRN = b - ((Bz * xR - Bz * b) / (Fcs(f(xR)) - Bz));
                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                System.out.printf("\nIteracion:%d  XRN: %f    Error Relativo: %f", i, xRN, eR);
                                xR = xRN;
                                i++;
                            } while (eR >= e);
                        }
                        break;
                    case 6:
                        System.out.println("Ingresa el valor de a: ");
                        a = teclado.nextDouble();
                        System.out.println("Ingresa el valor de b: ");
                        b = teclado.nextDouble();
                        System.out.println("Ingresa el porcentaje de error: ");
                        e = teclado.nextDouble();

                        Az = Fct(f(a));
                        Bz = Fct(f(b));

                        xR=(Az*b - Bz*a)/ (Az-Bz);
                        i=0;

                        if(Az*Fct(f(xR))==0) {
                            System.out.printf("La raiz es: %f", b);
                        } else if(Az*Fct(f(xR))<0) {
                            Bz=xR;
                        } else {
                            do {
                                xRN = b - ((Bz * xR - Bz * b) / (Fct(f(xR)) - Bz));
                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                System.out.printf("\nIteracion:%d  XRN: %f    Error Relativo: %f", i, xRN, eR);
                                xR = xRN;
                                i++;
                            } while (eR >= e);
                        }
                        break;
                default:
                    System.out.println("Valores invalidos.");
                    break;
                }
                break;
            case 2:
                switch (op2) {
                    case 1:
                        System.out.println("Ingresa el valor de a: ");
                        a = teclado.nextDouble();
                        System.out.println("Ingresa el valor de b: ");
                        b = teclado.nextDouble();
                        System.out.println("Ingresa el porcentaje de error: ");
                        e = teclado.nextDouble();

                        ax = Math.toRadians(a);
                        bx = Math.toRadians(b);

                        Az=Fs(f(ax));
                        Bz=Fs(f(bx));

                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                        System.out.printf("XR: %f ",xR);

                        if(Az*Fs(f(xR))==0) {
                            System.out.printf("La raiz es: %f", b);
                        } else if(Az*Fs(f(xR))<0) {
                            Bz=xR;
                        } else {
                            do {
                                xRN = bx - ((Bz*xR -Bz*bx) / (Fs(f(xR)) - Bz));
                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                System.out.printf("\nXRN: %f   Error Relativo: %f", xRN, eR);
                                xR = xRN;
                            } while (eR >= e);
                        }
                        break;
                    case 2:
                        System.out.println("Ingresa el valor de a: ");
                        a = teclado.nextDouble();
                        System.out.println("Ingresa el valor de b: ");
                        b = teclado.nextDouble();
                        System.out.println("Ingresa el porcentaje de error: ");
                        e = teclado.nextDouble();

                        ax = Math.toRadians(a);
                        bx = Math.toRadians(b);

                        Az=Fc(f(ax));
                        Bz=Fc(f(bx));

                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                        System.out.printf("XR: %f ",xR);

                        if(Az*Fc(f(xR))==0) {
                            System.out.printf("La raiz es: %f", b);
                        } else if(Az*Fc(f(xR))<0) {
                            Bz=xR;
                        } else {
                            do {
                                xRN = bx - ((Bz*xR -Bz*bx) / (Fc(f(xR)) - Bz));
                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                System.out.printf("\nXRN: %f   Error Relativo: %f", xRN, eR);
                                xR = xRN;
                            } while (eR >= e);
                        }
                    case 3:
                        System.out.println("Ingresa el valor de a: ");
                        a = teclado.nextDouble();
                        System.out.println("Ingresa el valor de b: ");
                        b = teclado.nextDouble();
                        System.out.println("Ingresa el porcentaje de error: ");
                        e = teclado.nextDouble();

                        ax = Math.toRadians(a);
                        bx = Math.toRadians(b);

                        Az=Ft(f(ax));
                        Bz=Ft(f(bx));

                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                        System.out.printf("XR: %f ",xR);

                        if(Az*Ft(f(xR))==0) {
                            System.out.printf("La raiz es: %f", b);
                        } else if(Az*Ft(f(xR))<0) {
                            Bz=xR;
                        } else {
                            do {
                                xRN = bx - ((Bz*xR -Bz*bx) / (Ft(f(xR)) - Bz));
                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                System.out.printf("\nXRN: %f   Error Relativo: %f", xRN, eR);
                                xR = xRN;
                            } while (eR >= e);
                        }
                        break;
                    case 4:
                        System.out.println("Ingresa el valor de a: ");
                        a = teclado.nextDouble();
                        System.out.println("Ingresa el valor de b: ");
                        b = teclado.nextDouble();
                        System.out.println("Ingresa el porcentaje de error: ");
                        e = teclado.nextDouble();

                        ax = Math.toRadians(a);
                        bx = Math.toRadians(b);

                        Az=Fsc(f(ax));
                        Bz=Fsc(f(bx));

                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                        System.out.printf("XR: %f ",xR);

                        if(Az*Fsc(f(xR))==0) {
                            System.out.printf("La raiz es: %f", b);
                        } else if(Az*Fsc(f(xR))<0) {
                            Bz=xR;
                        } else {
                            do {
                                xRN = bx - ((Bz*xR -Bz*bx) / (Fsc(f(xR)) - Bz));
                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                System.out.printf("\nXRN: %f   Error Relativo: %f", xRN, eR);
                                xR = xRN;
                            } while (eR >= e);
                        }
                        break;
                    case 5:
                        System.out.println("Ingresa el valor de a: ");
                        a = teclado.nextDouble();
                        System.out.println("Ingresa el valor de b: ");
                        b = teclado.nextDouble();
                        System.out.println("Ingresa el porcentaje de error: ");
                        e = teclado.nextDouble();

                        ax = Math.toRadians(a);
                        bx = Math.toRadians(b);

                        Az=Fcs(f(ax));
                        Bz=Fcs(f(bx));

                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                        System.out.printf("XR: %f ",xR);

                        if(Az*Fcs(f(xR))==0) {
                            System.out.printf("La raiz es: %f", b);
                        } else if(Az*Fcs(f(xR))<0) {
                            Bz=xR;
                        } else {
                            do {
                                xRN = bx - ((Bz*xR -Bz*bx) / (Fcs(f(xR)) - Bz));
                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                System.out.printf("\nXRN: %f   Error Relativo: %f", xRN, eR);
                                xR = xRN;
                            } while (eR >= e);
                        }
                        break;
                    case 6:
                        System.out.println("Ingresa el valor de a: ");
                        a = teclado.nextDouble();
                        System.out.println("Ingresa el valor de b: ");
                        b = teclado.nextDouble();
                        System.out.println("Ingresa el porcentaje de error: ");
                        e = teclado.nextDouble();

                        ax = Math.toRadians(a);
                        bx = Math.toRadians(b);

                        Az=Fct(f(ax));
                        Bz=Fct(f(bx));

                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                        System.out.printf("XR: %f ",xR);

                        if(Az*Fct(f(xR))==0) {
                            System.out.printf("La raiz es: %f", b);
                        } else if(Az*Fct(f(xR))<0) {
                            Bz=xR;
                        } else {
                            do {
                                xRN = bx - ((Bz*xR -Bz*bx) / (Fct(f(xR)) - Bz));
                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                System.out.printf("\nXRN: %f   Error Relativo: %f", xRN, eR);
                                xR = xRN;
                            } while (eR >= e);
                        }
                        break;
                    default:
                        System.out.println("Datos invalidos.");
                        break;
                }
                break;
            default:
                System.out.println("Datos invalidos.");
                break;
        }
    }
}
