import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class mTrigonometrica {

    static double f(double x) {
        return ( ((3)*(Math.pow(x, 2))) - ((5)*(x)) + (2) );
    }

    //seno
    static double Fs(double f){return Math.sin(f);}
    //coseno
    static double Fc(double f){return Math.cos(f);}
    //tangente
    static double Ft(double f){return Math.tan(f);}
    //secante
    static double Fsc(double f){return 1/(Math.cos(f));}
    //cosecante
    static double Fcs(double f){return 1/(Math.sin(f));}
    //cotangente
    static double Fct(double f){return 1/(Math.tan(f));}

    private JButton btnRespuesta, btnLimpiar;
    private JTextField txtValorA, txtDec, txtPorc, txtValorB;
    private JLabel lblValorA, lblDec, lblPorc, lblValorB, lblConversion, lblTrig;
    private JTextArea txaRes;
    private JRadioButton rtnDegrade, rtnRadian, rtnSin, rtnCos, rtnTan, rtnSec, rtnCsc, rtnCot;
    private KeyListener numeros, numeros2, numeros3, numeros4;
    private ActionListener actRes, actLimpiar;
    boolean bandera = false;

    public mTrigonometrica() {

        JFrame pnlMetodo = new JFrame("Metodo Biseccion");
        pnlMetodo.setSize(450, 500);
        pnlMetodo.setVisible(true);
        pnlMetodo.setLayout(null);
        pnlMetodo.setDefaultCloseOperation(pnlMetodo.EXIT_ON_CLOSE);

        //Introduccion de datos
        lblValorA = new JLabel("Valor del intervalo A: ");
        lblValorA.setBounds(20, 20, 150, 30);
        pnlMetodo.add(lblValorA);

        txtValorA = new JTextField();
        txtValorA.setBounds(200, 20, 150, 30);
        pnlMetodo.add(txtValorA);

        lblValorB = new JLabel("Valor del intervalo B: ");
        lblValorB.setBounds(20, 67, 350, 30);
        pnlMetodo.add(lblValorB);

        txtValorB = new JTextField();
        txtValorB.setBounds(200, 65, 150, 30);
        pnlMetodo.add(txtValorB);

        lblPorc = new JLabel("Porcentaje de error: ");
        lblPorc.setBounds(20, 110, 150, 30);
        pnlMetodo.add(lblPorc);

        txtPorc = new JTextField();
        txtPorc.setBounds(200, 110, 150, 30);
        pnlMetodo.add(txtPorc);

        lblDec = new JLabel("Decimales (0-9): ");
        lblDec.setBounds(20, 150, 150, 30);
        pnlMetodo.add(lblDec);

        txtDec = new JTextField();
        txtDec.setBounds(200, 150, 150, 30);
        pnlMetodo.add(txtDec);

        //Selleccion del tipo de datos
        lblConversion = new JLabel("Selecciona el tipo de dato: ");
        lblConversion.setBounds(20, 200, 170, 30);
        pnlMetodo.add(lblConversion);

        ButtonGroup rbg = new ButtonGroup();
        rtnDegrade = new JRadioButton("Degrados");
        rtnDegrade.setBounds(30, 235, 100, 30 );
        rtnDegrade.setSelected(true);
        rbg.add(rtnDegrade);
        pnlMetodo.add(rtnDegrade);

        rtnRadian = new JRadioButton("Radianes");
        rtnRadian.setBounds(30, 265, 100, 30 );
        rbg.add(rtnRadian);
        pnlMetodo.add(rtnRadian);

        //Operacion trignonometrica
        lblTrig = new JLabel("Selecciona la funcion que acompaña: ");
        lblTrig.setBounds(190, 200, 210, 30);
        pnlMetodo.add(lblTrig);

        ButtonGroup rbg2 = new ButtonGroup();
        rtnSin = new JRadioButton("Seno");
        rtnSin.setBounds(210, 230, 80, 25 );
        rtnSin.setSelected(true);
        rbg2.add(rtnSin);
        pnlMetodo.add(rtnSin);

        rtnCos = new JRadioButton("Coseno");
        rtnCos.setBounds(210, 255, 80, 25 );
        rbg2.add(rtnCos);
        pnlMetodo.add(rtnCos);

        rtnTan = new JRadioButton("Tangente");
        rtnTan.setBounds(210, 280, 80, 25 );
        rbg2.add(rtnTan);
        pnlMetodo.add(rtnTan);

        rtnSec = new JRadioButton("Secante");
        rtnSec.setBounds(300, 230, 100, 25 );
        rbg2.add(rtnSec);
        pnlMetodo.add(rtnSec);

        rtnCsc = new JRadioButton("Cosecante");
        rtnCsc.setBounds(300, 255, 100, 25 );
        rbg2.add(rtnCsc);
        pnlMetodo.add(rtnCsc);

        rtnCot = new JRadioButton("Cotangente");
        rtnCot.setBounds(300, 280, 100, 25 );
        rbg2.add(rtnCot);
        pnlMetodo.add(rtnCot);

        //Botones de calculo
        btnRespuesta = new JButton("Calcular");
        btnRespuesta.setBounds(190, 315, 100, 30);
        pnlMetodo.add(btnRespuesta);

        btnLimpiar = new JButton("Limpiar");
        btnLimpiar.setBounds(80, 315, 100, 30);
        pnlMetodo.add(btnLimpiar);

        //Area de respuesta
        txaRes = new JTextArea();
        txaRes.setBounds(20, 355, 345, 90);
        pnlMetodo.add(txaRes);

        JScrollPane scroll = new JScrollPane(txaRes);
        scroll.setBounds(20, 355, 345, 90);
        pnlMetodo.add(scroll);

        numeros = new KeyListener() {
            @Override
            public void keyTyped(KeyEvent k) {
                char car = k.getKeyChar();
                if (!(car>='0' && car<='9')) {
                    if (!(car == '.' )) {
                        if (!(car == '-' )) {
                            k.consume();
                        }
                    }
                }
                if(car == '.' && (txtValorA.getText().contains("."))){
                    k.consume();
                }
            }


            @Override
            public void keyPressed(KeyEvent keyEvent) {

            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {

            }
        };
        txtValorA.addKeyListener(numeros);

        numeros2 = new KeyListener() {
            @Override
            public void keyTyped(KeyEvent k) {
                char car = k.getKeyChar();
                if (!(car>='0' && car<='9')) {
                    if (!(car == '.')) {
                        if (!(car == '-' )) {
                            k.consume();
                        }
                    }
                }
            }


            @Override
            public void keyPressed(KeyEvent keyEvent) {

            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {

            }
        };
        txtValorB.addKeyListener(numeros2);

        numeros3 = new KeyListener() {
            @Override
            public void keyTyped(KeyEvent k) {
                char car = k.getKeyChar();
                if (!(car>='0' && car<='9')) {
                    if (!(car == '.')) {
                        k.consume();
                    }
                }
                if(car == '.' && (txtPorc.getText().contains("."))){
                    k.consume();
                }
            }


            @Override
            public void keyPressed(KeyEvent keyEvent) {

            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {

            }
        };
        txtPorc.addKeyListener(numeros3);

        numeros4 = new KeyListener() {
            @Override
            public void keyTyped(KeyEvent k) {
                char car = k.getKeyChar();
                if (!(car>='0' && car<='9')) {
                    k.consume();
                }
            }


            @Override
            public void keyPressed(KeyEvent keyEvent) {

            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {

            }
        };
        txtDec.addKeyListener(numeros4);

        actRes = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (!bandera) {
                    String a1 = txtValorA.getText().trim();
                    if (a1.isEmpty()) {
                        JOptionPane.showMessageDialog(null, "El campo 1 no puede estar vacio.");
                    } else {
                        String b1 = txtValorB.getText().trim();
                        if (b1.isEmpty()) {
                            JOptionPane.showMessageDialog(null, "El campo 2 no puede estar vacio.");
                        } else {
                            String p = txtPorc.getText().trim();
                            if (p.isEmpty()) {
                                JOptionPane.showMessageDialog(null, "El campo 3 no puede estar vacio.");
                            } else {
                                String d = txtDec.getText().trim();
                                if (d.isEmpty()) {
                                    JOptionPane.showMessageDialog(null, "El campo 4 no puede estar vacio.");
                                } else {
                                    double a = Double.parseDouble(a1);
                                    double b = Double.parseDouble(b1);
                                    double e = Double.parseDouble(p);
                                    int dec = Integer.parseInt(d), i=0;
                                    double xR, xRN, eR, Az, Bz, ax, bx;

                                    if (e <= 100) {
                                        if (dec >= 1 && dec <= 9) {
                                            bandera = true;
                                            if (dec == 1) {
                                                if(rtnRadian.isSelected()){
                                                    if(rtnSin.isSelected()){
                                                        Az = Fs(f(a));
                                                        Bz = Fs(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);

                                                        if(Az*Fs(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fs(f(xR))<0) {
                                                            ////Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fs(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.1f     Error rel: %.1f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCos.isSelected()){
                                                        Az = Fc(f(a));
                                                        Bz = Fc(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fc(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fc(f(xR))<0) {
                                                            ////Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fc(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.1f     Error rel: %.1f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnTan.isSelected()){
                                                        Az = Ft(f(a));
                                                        Bz = Ft(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Ft(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Ft(f(xR))<0) {
                                                            ////Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Ft(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.1f     Error rel: %.1f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnSec.isSelected()){
                                                        Az = Fsc(f(a));
                                                        Bz = Fsc(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fsc(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fsc(f(xR))<0) {
                                                            ////Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fsc(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.1f     Error rel: %.1f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }                                                    }
                                                    if(rtnCsc.isSelected()){
                                                        Az = Fcs(f(a));
                                                        Bz = Fcs(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fcs(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fcs(f(xR))<0) {
                                                            ////Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fcs(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.1f     Error rel: %.1f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCot.isSelected()){
                                                        Az = Fct(f(a));
                                                        Bz = Fct(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fct(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fct(f(xR))<0) {
                                                            ////Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fct(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.1f     Error rel: %.1f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                }
                                                if(rtnDegrade.isSelected()){
                                                    if(rtnSin.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fs(f(ax));
                                                        Bz=Fs(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fs(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fs(f(xR))<0) {
                                                            ////Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Fs(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.1f     Error rel: %.1f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCos.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fc(f(ax));
                                                        Bz=Fc(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fc(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fc(f(xR))<0) {
                                                            ////Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz * xR - Bz * bx) / (Fc(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.1f     Error rel: %.1f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnTan.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Ft(f(ax));
                                                        Bz=Ft(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);
                                                        i=0;

                                                         if(Az*Ft(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Ft(f(xR))<0) {
                                                            ////Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Ft(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.1f     Error rel: %.1f\n\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnSec.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fsc(f(ax));
                                                        Bz=Fsc(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fsc(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fsc(f(xR))<0) {
                                                            ////Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Fsc(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.1f     Error rel: %.1f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCsc.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fcs(f(ax));
                                                        Bz=Fcs(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fcs(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fcs(f(xR))<0) {
                                                            ////Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Fcs(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.1f     Error rel: %.1f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCot.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fct(f(ax));
                                                        Bz=Fct(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fct(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fct(f(xR))<0) {
                                                            ////Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Fct(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.1f     Error rel: %.1f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                }
                                            } else if (dec == 2) {
                                                if(rtnRadian.isSelected()){
                                                    if(rtnSin.isSelected()){
                                                        Az = Fs(f(a));
                                                        Bz = Fs(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fs(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fs(f(xR))<0) {
                                                            ////Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fs(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.2f     Error rel: %.2f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCos.isSelected()){
                                                        Az = Fc(f(a));
                                                        Bz = Fc(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fc(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fc(f(xR))<0) {
                                                            ////Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fc(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.2f     Error rel: %.2f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnTan.isSelected()){
                                                        Az = Ft(f(a));
                                                        Bz = Ft(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Ft(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Ft(f(xR))<0) {
                                                            ////Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Ft(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.2f     Error rel: %.2f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnSec.isSelected()){
                                                        Az = Fsc(f(a));
                                                        Bz = Fsc(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fsc(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fsc(f(xR))<0) {
                                                            ////Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fsc(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.2f     Error rel: %.2f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }                                                    }
                                                    if(rtnCsc.isSelected()){
                                                        Az = Fcs(f(a));
                                                        Bz = Fcs(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fcs(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fcs(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fcs(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.2f     Error rel: %.2f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCot.isSelected()){
                                                        Az = Fct(f(a));
                                                        Bz = Fct(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fct(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fct(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fct(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.2f     Error rel: %.2f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                }
                                                if(rtnDegrade.isSelected()){
                                                    if(rtnSin.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fs(f(ax));
                                                        Bz=Fs(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fs(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fs(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Fs(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.2f     Error rel: %.2f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCos.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fc(f(ax));
                                                        Bz=Fc(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fc(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fc(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz * xR - Bz * bx) / (Fc(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.2f     Error rel: %.2f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnTan.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Ft(f(ax));
                                                        Bz=Ft(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Ft(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Ft(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Ft(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.2f     Error rel: %.2f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnSec.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fsc(f(ax));
                                                        Bz=Fsc(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fsc(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fsc(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Fsc(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.2f     Error rel: %.2f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCsc.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fcs(f(ax));
                                                        Bz=Fcs(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fcs(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fcs(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Fcs(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.2f     Error rel: %.2f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCot.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fct(f(ax));
                                                        Bz=Fct(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fct(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fct(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Fct(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.2f     Error rel: %.2f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                }
                                            } else if (dec == 3) {
                                                if(rtnRadian.isSelected()){
                                                    if(rtnSin.isSelected()){
                                                        Az = Fs(f(a));
                                                        Bz = Fs(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fs(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fs(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fs(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.3f     Error rel: %.3f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCos.isSelected()){
                                                        Az = Fc(f(a));
                                                        Bz = Fc(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fc(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fc(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fc(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.3f     Error rel: %.3f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnTan.isSelected()){
                                                        Az = Ft(f(a));
                                                        Bz = Ft(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Ft(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Ft(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Ft(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.3f     Error rel: %.3f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnSec.isSelected()){
                                                        Az = Fsc(f(a));
                                                        Bz = Fsc(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fsc(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fsc(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fsc(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.3f     Error rel: %.3f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }                                                    }
                                                    if(rtnCsc.isSelected()){
                                                        Az = Fcs(f(a));
                                                        Bz = Fcs(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fcs(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fcs(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fcs(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.3f     Error rel: %.3f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCot.isSelected()){
                                                        Az = Fct(f(a));
                                                        Bz = Fct(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fct(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fct(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fct(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.3f     Error rel: %.3f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                }
                                                if(rtnDegrade.isSelected()){
                                                    if(rtnSin.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fs(f(ax));
                                                        Bz=Fs(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fs(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fs(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Fs(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.3f     Error rel: %.3f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCos.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fc(f(ax));
                                                        Bz=Fc(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fc(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fc(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz * xR - Bz * bx) / (Fc(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.3f     Error rel: %.3f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnTan.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Ft(f(ax));
                                                        Bz=Ft(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Ft(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Ft(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Ft(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.3f     Error rel: %.3f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnSec.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fsc(f(ax));
                                                        Bz=Fsc(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fsc(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fsc(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Fsc(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.3f     Error rel: %.3f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCsc.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fcs(f(ax));
                                                        Bz=Fcs(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fcs(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fcs(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Fcs(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.3f     Error rel: %.3f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCot.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fct(f(ax));
                                                        Bz=Fct(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fct(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fct(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Fct(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.3f     Error rel: %.3f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                }
                                            } else if (dec == 4) {
                                                if(rtnRadian.isSelected()){
                                                    if(rtnSin.isSelected()){
                                                        Az = Fs(f(a));
                                                        Bz = Fs(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fs(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fs(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fs(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.4f     Error rel: %.4f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCos.isSelected()){
                                                        Az = Fc(f(a));
                                                        Bz = Fc(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fc(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fc(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fc(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.4f     Error rel: %.4f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnTan.isSelected()){
                                                        Az = Ft(f(a));
                                                        Bz = Ft(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Ft(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.4f     Error rel: %.4f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                    }
                                                    if(rtnSec.isSelected()){
                                                        Az = Fsc(f(a));
                                                        Bz = Fsc(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fsc(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fsc(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fsc(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.4f     Error rel: %.4f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }                                                    }
                                                    if(rtnCsc.isSelected()){
                                                        Az = Fcs(f(a));
                                                        Bz = Fcs(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fcs(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fcs(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fcs(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.4f     Error rel: %.4f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCot.isSelected()){
                                                        Az = Fct(f(a));
                                                        Bz = Fct(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fct(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fct(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fct(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.4f     Error rel: %.4f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                }
                                                if(rtnDegrade.isSelected()){
                                                    if(rtnSin.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fs(f(ax));
                                                        Bz=Fs(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fs(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fs(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Fs(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.4f     Error rel: %.4f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCos.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fc(f(ax));
                                                        Bz=Fc(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fc(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fc(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz * xR - Bz * bx) / (Fc(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.4f     Error rel: %.4f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnTan.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Ft(f(ax));
                                                        Bz=Ft(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Ft(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.4f     Error rel: %.4f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);

                                                    }
                                                    if(rtnSec.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fsc(f(ax));
                                                        Bz=Fsc(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fsc(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fsc(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Fsc(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.4f     Error rel: %.4f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCsc.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fcs(f(ax));
                                                        Bz=Fcs(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fcs(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fcs(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Fcs(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.4f     Error rel: %.4f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCot.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fct(f(ax));
                                                        Bz=Fct(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fct(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fct(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Fct(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.4f     Error rel: %.4f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                }
                                            } else if (dec == 5) {
                                                if(rtnRadian.isSelected()){
                                                    if(rtnSin.isSelected()){
                                                        Az = Fs(f(a));
                                                        Bz = Fs(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fs(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fs(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fs(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.5f     Error rel: %.5f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCos.isSelected()){
                                                        Az = Fc(f(a));
                                                        Bz = Fc(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fc(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fc(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fc(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.5f     Error rel: %.5f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnTan.isSelected()){
                                                        Az = Ft(f(a));
                                                        Bz = Ft(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Ft(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Ft(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Ft(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.5f     Error rel: %.5f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnSec.isSelected()){
                                                        Az = Fsc(f(a));
                                                        Bz = Fsc(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fsc(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fsc(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fsc(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.5f     Error rel: %.5f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }                                                    }
                                                    if(rtnCsc.isSelected()){
                                                        Az = Fcs(f(a));
                                                        Bz = Fcs(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fcs(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fcs(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fcs(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.5f     Error rel: %.5f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCot.isSelected()){
                                                        Az = Fct(f(a));
                                                        Bz = Fct(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fct(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fct(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fct(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.5f     Error rel: %.5f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                }
                                                if(rtnDegrade.isSelected()){
                                                    if(rtnSin.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fs(f(ax));
                                                        Bz=Fs(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fs(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fs(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Fs(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.5f     Error rel: %.5f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCos.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fc(f(ax));
                                                        Bz=Fc(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fc(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fc(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz * xR - Bz * bx) / (Fc(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.5f     Error rel: %.5f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnTan.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Ft(f(ax));
                                                        Bz=Ft(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Ft(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Ft(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Ft(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.5f     Error rel: %.5f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnSec.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fsc(f(ax));
                                                        Bz=Fsc(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fsc(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fsc(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Fsc(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.5f     Error rel: %.5f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCsc.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fcs(f(ax));
                                                        Bz=Fcs(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fcs(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fcs(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Fcs(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.5f     Error rel: %.5f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCot.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fct(f(ax));
                                                        Bz=Fct(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fct(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fct(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Fct(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.5f     Error rel: %.5f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                }
                                            } else if (dec == 6) {
                                                if(rtnRadian.isSelected()){
                                                    if(rtnSin.isSelected()){
                                                        Az = Fs(f(a));
                                                        Bz = Fs(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fs(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fs(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fs(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.6f     Error rel: %.6f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCos.isSelected()){
                                                        Az = Fc(f(a));
                                                        Bz = Fc(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fc(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fc(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fc(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.6f     Error rel: %.6f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnTan.isSelected()){
                                                        Az = Ft(f(a));
                                                        Bz = Ft(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Ft(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Ft(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Ft(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.6f     Error rel: %.6f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnSec.isSelected()){
                                                        Az = Fsc(f(a));
                                                        Bz = Fsc(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fsc(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fsc(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fsc(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.6f     Error rel: %.6f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }                                                    }
                                                    if(rtnCsc.isSelected()){
                                                        Az = Fcs(f(a));
                                                        Bz = Fcs(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fcs(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fcs(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fcs(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.6f     Error rel: %.6f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCot.isSelected()){
                                                        Az = Fct(f(a));
                                                        Bz = Fct(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fct(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fct(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fct(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.6f     Error rel: %.6f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                }
                                                if(rtnDegrade.isSelected()){
                                                    if(rtnSin.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fs(f(ax));
                                                        Bz=Fs(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fs(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fs(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Fs(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.6f     Error rel: %.6f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCos.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fc(f(ax));
                                                        Bz=Fc(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fc(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fc(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz * xR - Bz * bx) / (Fc(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.6f     Error rel: %.6f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnTan.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Ft(f(ax));
                                                        Bz=Ft(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Ft(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Ft(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Ft(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.6f     Error rel: %.6f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnSec.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fsc(f(ax));
                                                        Bz=Fsc(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fsc(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fsc(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Fsc(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.6f     Error rel: %.6f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCsc.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fcs(f(ax));
                                                        Bz=Fcs(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fcs(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fcs(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Fcs(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.6f     Error rel: %.6f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCot.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fct(f(ax));
                                                        Bz=Fct(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fct(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fct(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Fct(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.6f     Error rel: %.6f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                }
                                            } else if (dec == 7) {
                                                if(rtnRadian.isSelected()){
                                                    if(rtnSin.isSelected()){
                                                        Az = Fs(f(a));
                                                        Bz = Fs(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fs(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fs(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fs(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.7f     Error rel: %.7f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCos.isSelected()){
                                                        Az = Fc(f(a));
                                                        Bz = Fc(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fc(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fc(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fc(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.7f     Error rel: %.7f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnTan.isSelected()){
                                                        Az = Ft(f(a));
                                                        Bz = Ft(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Ft(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Ft(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Ft(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.7f     Error rel: %.7f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnSec.isSelected()){
                                                        Az = Fsc(f(a));
                                                        Bz = Fsc(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fsc(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fsc(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fsc(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.7f     Error rel: %.7f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }                                                    }
                                                    if(rtnCsc.isSelected()){
                                                        Az = Fcs(f(a));
                                                        Bz = Fcs(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fcs(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fcs(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fcs(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.7f     Error rel: %.7f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCot.isSelected()){
                                                        Az = Fct(f(a));
                                                        Bz = Fct(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fct(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fct(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fct(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.7f     Error rel: %.7f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                }
                                                if(rtnDegrade.isSelected()){
                                                    if(rtnSin.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fs(f(ax));
                                                        Bz=Fs(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fs(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fs(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Fs(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.7f     Error rel: %.7f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCos.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fc(f(ax));
                                                        Bz=Fc(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fc(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fc(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz * xR - Bz * bx) / (Fc(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.7f     Error rel: %.7f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnTan.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Ft(f(ax));
                                                        Bz=Ft(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Ft(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Ft(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Ft(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.7f     Error rel: %.7f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnSec.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fsc(f(ax));
                                                        Bz=Fsc(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fsc(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fsc(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Fsc(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.7f     Error rel: %.7f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCsc.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fcs(f(ax));
                                                        Bz=Fcs(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fcs(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fcs(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Fcs(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.7f     Error rel: %.7f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCot.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fct(f(ax));
                                                        Bz=Fct(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fct(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fct(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Fct(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.7f     Error rel: %.7f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                }
                                            } else if (dec == 8) {
                                                if(rtnRadian.isSelected()){
                                                    if(rtnSin.isSelected()){
                                                        Az = Fs(f(a));
                                                        Bz = Fs(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fs(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fs(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fs(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.8f     Error rel: %.8f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCos.isSelected()){
                                                        Az = Fc(f(a));
                                                        Bz = Fc(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fc(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fc(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fc(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.8f     Error rel: %.8f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnTan.isSelected()){
                                                        Az = Ft(f(a));
                                                        Bz = Ft(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Ft(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Ft(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Ft(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.8f     Error rel: %.8f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnSec.isSelected()){
                                                        Az = Fsc(f(a));
                                                        Bz = Fsc(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fsc(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fsc(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fsc(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.8f     Error rel: %.8f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }                                                    }
                                                    if(rtnCsc.isSelected()){
                                                        Az = Fcs(f(a));
                                                        Bz = Fcs(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fcs(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fcs(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fcs(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.8f     Error rel: %.8f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCot.isSelected()){
                                                        Az = Fct(f(a));
                                                        Bz = Fct(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fct(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fct(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fct(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.8f     Error rel: %.8f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                }
                                                if(rtnDegrade.isSelected()){
                                                    if(rtnSin.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fs(f(ax));
                                                        Bz=Fs(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fs(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fs(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Fs(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.8f     Error rel: %.8f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCos.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fc(f(ax));
                                                        Bz=Fc(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fc(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fc(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz * xR - Bz * bx) / (Fc(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.8f     Error rel: %.8f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnTan.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Ft(f(ax));
                                                        Bz=Ft(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Ft(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Ft(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Ft(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.8f     Error rel: %.8f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnSec.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fsc(f(ax));
                                                        Bz=Fsc(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fsc(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fsc(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Fsc(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.8f     Error rel: %.8f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCsc.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fcs(f(ax));
                                                        Bz=Fcs(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fcs(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fcs(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Fcs(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.8f     Error rel: %.8f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCot.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fct(f(ax));
                                                        Bz=Fct(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fct(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fct(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Fct(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.8f     Error rel: %.8f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                }
                                            } else if (dec == 9) {
                                                if(rtnRadian.isSelected()){
                                                    if(rtnSin.isSelected()){
                                                        Az = Fs(f(a));
                                                        Bz = Fs(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fs(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fs(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fs(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.9f     Error rel: %.9f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCos.isSelected()){
                                                        Az = Fc(f(a));
                                                        Bz = Fc(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fc(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fc(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fc(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.9f     Error rel: %.9f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnTan.isSelected()){
                                                        Az = Ft(f(a));
                                                        Bz = Ft(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Ft(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Ft(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Ft(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.9f     Error rel: %.9f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnSec.isSelected()){
                                                        Az = Fsc(f(a));
                                                        Bz = Fsc(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fsc(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fsc(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fsc(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.9f     Error rel: %.9f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }                                                    }
                                                    if(rtnCsc.isSelected()){
                                                        Az = Fcs(f(a));
                                                        Bz = Fcs(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fcs(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fcs(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fcs(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.9f     Error rel: %.9f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCot.isSelected()){
                                                        Az = Fct(f(a));
                                                        Bz = Fct(f(b));

                                                        xR=(Az*b - Bz*a)/ (Az-Bz);
                                                        i=0;

                                                        if(Az*Fct(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fct(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = b - ((Bz * xR - Bz * b) / (Fct(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.9f     Error rel: %.9f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                }
                                                if(rtnDegrade.isSelected()){
                                                    if(rtnSin.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fs(f(ax));
                                                        Bz=Fs(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fs(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fs(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Fs(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.9f     Error rel: %.9f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCos.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fc(f(ax));
                                                        Bz=Fc(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fc(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fc(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz * xR - Bz * bx) / (Fc(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.9f     Error rel: %.9f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnTan.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Ft(f(ax));
                                                        Bz=Ft(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Ft(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Ft(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Ft(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.9f     Error rel: %.9f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnSec.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fsc(f(ax));
                                                        Bz=Fsc(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fsc(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fsc(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Fsc(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.9f     Error rel: %.9f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCsc.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fcs(f(ax));
                                                        Bz=Fcs(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fcs(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fcs(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Fcs(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.9f     Error rel: %.9f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                    if(rtnCot.isSelected()){
                                                        ax = Math.toRadians(a);
                                                        bx = Math.toRadians(b);

                                                        Az=Fct(f(ax));
                                                        Bz=Fct(f(bx));

                                                        xR = (Az*bx - Bz*ax)/ (Az-Bz);
                                                        System.out.printf("XR: %f ",xR);

                                                        if(Az*Fct(f(xR))==0) {
                                                            System.out.printf("La raiz es: %f", b);
                                                        } else if(Az*Fct(f(xR))<0) {
                                                            //Bz=xR;
                                                        } else {
                                                            do {
                                                                xRN = bx - ((Bz*xR -Bz*bx) / (Fct(f(xR)) - Bz));
                                                                eR = Math.abs((xRN - xR) / (xRN)) * 100;
                                                                txaRes.append(String.format("Iteracion: %d    Raiz: %.9f     Error rel: %.9f\n", i+1, xRN, eR));
                                                                xR = xRN;
                                                                i++;
                                                            } while (eR >= e);
                                                        }
                                                    }
                                                }
                                            } else {
                                                JOptionPane.showMessageDialog(null, "Ingresa valores correctos.");
                                            }
                                        } else {
                                            JOptionPane.showMessageDialog(null, "Los valores deben ser entre 1 y 9.");
                                        }
                                    } else {
                                        JOptionPane.showMessageDialog(null, "El error debe ser menor a 100.");
                                    }
                                }
                            }
                        }
                    }
                }else{
                    JOptionPane.showMessageDialog(null, "Primero limpia la pantalla.");
                }
            }
        };
        btnRespuesta.addActionListener(actRes);

        actLimpiar = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                bandera=false;
                txtValorA.setText("");
                txtValorB.setText("");
                txtPorc.setText("");
                txtDec.setText("");
                txaRes.setText("");
            }
        };
        btnLimpiar.addActionListener(actLimpiar);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new mTrigonometrica();
            }
        });
    }

}