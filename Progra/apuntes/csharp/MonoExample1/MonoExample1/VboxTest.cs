using System;
using Gtk;

public partial class VboxTest : Gtk.Window
{
	public VboxTest () : base (Gtk.WindowType.Toplevel)
	{
		Build ();
	}

	protected void OnBtnAceptarClicked (object sender, EventArgs e)
	{
		if ( entryMensaje.Text.Length != 0) 
		{
			MessageDialog md1 = new MessageDialog (null, DialogFlags.Modal, 
			                                       MessageType.Info, 
			                                       ButtonsType.Ok, 
			                                       "Hello: " + entryMensaje.Text);
			md1.Run (); 
			md1.Destroy ();
		}
    }

	protected void OnEntryMensajeChanged (object sender, EventArgs e)
	{
		if (entryMensaje.Text == "") {
			MessageDialog md1 = new MessageDialog (null, DialogFlags.Modal, 
			                                       MessageType.Info, 
			                                       ButtonsType.Ok, 
			                                       "Entry is empty");
			md1.Run (); 
			md1.Destroy ();
		} else {
			lblDisplay.Text = entryMensaje.Text;
		}
	}
}