using System;
using Gtk;

namespace MonoExample1
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Application.Init ();

			Menu menu = new Menu ();
			menu.Show ();

			Application.Run ();
		}
	}
}
