﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace relojChecador
{
    public partial class Form1 : Form
    {
        public string registro = @"e:\\RelojChecador.txt";
        public Form1()
        {
            InitializeComponent();
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            if (!File.Exists(registro))
            {
                using (StreamWriter sw = File.CreateText(registro))
                {
                    sw.WriteLine(cmbEmpleado.SelectedItem.ToString());
                    sw.WriteLine(dtpFecha.Value.ToString());
                    if (rbtnEntrada.Checked)
                        sw.WriteLine("Entrada");
                    else if (rbtnSalida.Checked)
                        sw.WriteLine("Salida");
                }
            }
        }

        private void btnRevisar_Click(object sender, EventArgs e)
        {
            string readText = File.ReadAllText(registro);
            MessageBox.Show(readText);

        }
    }
}
