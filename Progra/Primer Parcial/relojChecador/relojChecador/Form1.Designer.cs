﻿namespace relojChecador
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbEmpleado = new System.Windows.Forms.ComboBox();
            this.lblEmpleado = new System.Windows.Forms.Label();
            this.lblFecha = new System.Windows.Forms.Label();
            this.dtpFecha = new System.Windows.Forms.DateTimePicker();
            this.rbtnEntrada = new System.Windows.Forms.RadioButton();
            this.rbtnSalida = new System.Windows.Forms.RadioButton();
            this.gpbOperacion = new System.Windows.Forms.GroupBox();
            this.btnRegistrar = new System.Windows.Forms.Button();
            this.btnRevisar = new System.Windows.Forms.Button();
            this.gpbOperacion.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmbEmpleado
            // 
            this.cmbEmpleado.FormattingEnabled = true;
            this.cmbEmpleado.Items.AddRange(new object[] {
            "Edgar Omar",
            "Carlos Ivan",
            "Luis Carlos"});
            this.cmbEmpleado.Location = new System.Drawing.Point(141, 49);
            this.cmbEmpleado.Name = "cmbEmpleado";
            this.cmbEmpleado.Size = new System.Drawing.Size(387, 21);
            this.cmbEmpleado.TabIndex = 0;
            // 
            // lblEmpleado
            // 
            this.lblEmpleado.AutoSize = true;
            this.lblEmpleado.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.lblEmpleado.Location = new System.Drawing.Point(45, 52);
            this.lblEmpleado.Name = "lblEmpleado";
            this.lblEmpleado.Size = new System.Drawing.Size(79, 18);
            this.lblEmpleado.TabIndex = 1;
            this.lblEmpleado.Text = "Empleado:";
            // 
            // lblFecha
            // 
            this.lblFecha.AutoSize = true;
            this.lblFecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.lblFecha.Location = new System.Drawing.Point(45, 111);
            this.lblFecha.Name = "lblFecha";
            this.lblFecha.Size = new System.Drawing.Size(90, 18);
            this.lblFecha.TabIndex = 2;
            this.lblFecha.Text = "Fecha/Hora:";
            // 
            // dtpFecha
            // 
            this.dtpFecha.Location = new System.Drawing.Point(141, 111);
            this.dtpFecha.Name = "dtpFecha";
            this.dtpFecha.Size = new System.Drawing.Size(361, 20);
            this.dtpFecha.TabIndex = 3;
            // 
            // rbtnEntrada
            // 
            this.rbtnEntrada.AutoSize = true;
            this.rbtnEntrada.Location = new System.Drawing.Point(13, 24);
            this.rbtnEntrada.Name = "rbtnEntrada";
            this.rbtnEntrada.Size = new System.Drawing.Size(62, 17);
            this.rbtnEntrada.TabIndex = 4;
            this.rbtnEntrada.TabStop = true;
            this.rbtnEntrada.Text = "Entrada";
            this.rbtnEntrada.UseVisualStyleBackColor = true;
            // 
            // rbtnSalida
            // 
            this.rbtnSalida.AutoSize = true;
            this.rbtnSalida.Location = new System.Drawing.Point(131, 24);
            this.rbtnSalida.Name = "rbtnSalida";
            this.rbtnSalida.Size = new System.Drawing.Size(54, 17);
            this.rbtnSalida.TabIndex = 5;
            this.rbtnSalida.TabStop = true;
            this.rbtnSalida.Text = "Salida";
            this.rbtnSalida.UseVisualStyleBackColor = true;
            // 
            // gpbOperacion
            // 
            this.gpbOperacion.Controls.Add(this.rbtnSalida);
            this.gpbOperacion.Controls.Add(this.rbtnEntrada);
            this.gpbOperacion.Location = new System.Drawing.Point(141, 167);
            this.gpbOperacion.Name = "gpbOperacion";
            this.gpbOperacion.Size = new System.Drawing.Size(328, 58);
            this.gpbOperacion.TabIndex = 6;
            this.gpbOperacion.TabStop = false;
            this.gpbOperacion.Text = "Operaciones";
            // 
            // btnRegistrar
            // 
            this.btnRegistrar.Location = new System.Drawing.Point(191, 254);
            this.btnRegistrar.Name = "btnRegistrar";
            this.btnRegistrar.Size = new System.Drawing.Size(81, 28);
            this.btnRegistrar.TabIndex = 7;
            this.btnRegistrar.Text = "Registrar";
            this.btnRegistrar.UseVisualStyleBackColor = true;
            this.btnRegistrar.Click += new System.EventHandler(this.btnRegistrar_Click);
            // 
            // btnRevisar
            // 
            this.btnRevisar.Location = new System.Drawing.Point(324, 254);
            this.btnRevisar.Name = "btnRevisar";
            this.btnRevisar.Size = new System.Drawing.Size(81, 28);
            this.btnRevisar.TabIndex = 8;
            this.btnRevisar.Text = "Revisar";
            this.btnRevisar.UseVisualStyleBackColor = true;
            this.btnRevisar.Click += new System.EventHandler(this.btnRevisar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(573, 336);
            this.Controls.Add(this.btnRevisar);
            this.Controls.Add(this.btnRegistrar);
            this.Controls.Add(this.gpbOperacion);
            this.Controls.Add(this.dtpFecha);
            this.Controls.Add(this.lblFecha);
            this.Controls.Add(this.lblEmpleado);
            this.Controls.Add(this.cmbEmpleado);
            this.Name = "Form1";
            this.Text = "Reloj Checador";
            this.gpbOperacion.ResumeLayout(false);
            this.gpbOperacion.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbEmpleado;
        private System.Windows.Forms.Label lblEmpleado;
        private System.Windows.Forms.Label lblFecha;
        private System.Windows.Forms.DateTimePicker dtpFecha;
        private System.Windows.Forms.RadioButton rbtnEntrada;
        private System.Windows.Forms.RadioButton rbtnSalida;
        private System.Windows.Forms.GroupBox gpbOperacion;
        private System.Windows.Forms.Button btnRegistrar;
        private System.Windows.Forms.Button btnRevisar;
    }
}

