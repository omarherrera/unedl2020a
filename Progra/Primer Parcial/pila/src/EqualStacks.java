import java.util.Scanner;
import java.util.Stack;

public class EqualStacks {
    static int equalStacks(int[] t1, int[] t2, int[] t3) {

        Stack<Integer> pila1 = new Stack<>();
        Stack<Integer> pila2 = new Stack<>();
        Stack<Integer> pila3 = new Stack<>();

        int pila1AlturaTotal = 0, pila2AlturaTotal = 0, pila3AlturaTotal = 0;

        for (int i = t1.length - 1; i >= 0; i--) {
            pila1AlturaTotal += t1[i];
            pila1.push(pila1AlturaTotal);
        }
        for (int i = t2.length - 1; i >= 0; i--) {
            pila2AlturaTotal += t2[i];
            pila2.push(pila2AlturaTotal);
        }
        for (int i = t3.length - 1; i >= 0; i--) {
            pila3AlturaTotal += t3[i];
            pila3.push(pila3AlturaTotal);
        }

        while (true) {

            if (pila1.isEmpty() || pila2.isEmpty() || pila3.isEmpty())
                return 0;

            pila1AlturaTotal = pila1.peek();
            pila2AlturaTotal = pila2.peek();
            pila3AlturaTotal = pila3.peek();

            if (pila1AlturaTotal == pila2AlturaTotal && pila2AlturaTotal == pila3AlturaTotal)
                return pila1AlturaTotal;

            if (pila1AlturaTotal >= pila2AlturaTotal && pila1AlturaTotal >= pila3AlturaTotal)
                pila1.pop();
            else if (pila2AlturaTotal >= pila3AlturaTotal && pila2AlturaTotal >= pila3AlturaTotal)
                pila2.pop();
            else if (pila3AlturaTotal >= pila2AlturaTotal && pila3AlturaTotal >= pila1AlturaTotal)
                pila3.pop();
        }

    }

    public static void main(String args[]) {
        Scanner leer = new Scanner(System.in);
        int n1 = leer.nextInt();
        int n2 = leer.nextInt();
        int n3 = leer.nextInt();
        int t1[] = new int[n1];
        for (int t1_i = 0; t1_i < n1; t1_i++) {
            t1[t1_i] = leer.nextInt();
        }
        int t2[] = new int[n2];
        for (int t2_i = 0; t2_i < n2; t2_i++) {
            t2[t2_i] = leer.nextInt();
        }
        int t3[] = new int[n3];
        for (int t3_i = 0; t3_i < n3; t3_i++) {
            t3[t3_i] = leer.nextInt();
        }
        System.out.println(equalStacks(t1, t2, t3));
        leer.close();
    }
}