﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Linq
{
    class Program
    {
       /* static void Main(string[] args)
        {
            var startingDeck = Suits().SelectMany(suit => Ranks().Select(rank => new { Suit = suit, Rank = rank }));

            // Display each card that we've generated and placed in startingDeck in the console
            foreach (var card in startingDeck)
            {
                Console.WriteLine(card);
            }
        }*/

        static IEnumerable<string> Suits()
        {
            yield return "treboles";
            yield return "diamantes";
            yield return "corazones";
            yield return "espadas";
        }

        static IEnumerable<string> Ranks()
        {
            yield return "dos";
            yield return "tres";
            yield return "cuatro";
            yield return "cinco";
            yield return "seis";
            yield return "siete";
            yield return "ocho";
            yield return "nueve";
            yield return "diez";
            yield return "joto";
            yield return "quina";
            yield return "rey";
            yield return "az";
        }

    }
}
