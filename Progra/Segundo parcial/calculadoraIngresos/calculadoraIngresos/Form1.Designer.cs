﻿namespace calculadoraIngresos
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSueldo = new System.Windows.Forms.Label();
            this.txtSueldo = new System.Windows.Forms.TextBox();
            this.gbxSueldo = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtNomina = new System.Windows.Forms.TextBox();
            this.lblDias = new System.Windows.Forms.Label();
            this.lblAguinaldo = new System.Windows.Forms.Label();
            this.lblRAguinaldo = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblRVacaciones = new System.Windows.Forms.Label();
            this.lblRInfonavit = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblRIMSS = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblRRCV = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lblRSeguro = new System.Windows.Forms.Label();
            this.lblRVales = new System.Windows.Forms.Label();
            this.lblRSGMM = new System.Windows.Forms.Label();
            this.lblSeguroV = new System.Windows.Forms.Label();
            this.lblVales = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.lblRSueldo = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtVacaciones = new System.Windows.Forms.TextBox();
            this.lblVaciones = new System.Windows.Forms.Label();
            this.gbxSueldo.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblSueldo
            // 
            this.lblSueldo.AutoSize = true;
            this.lblSueldo.Location = new System.Drawing.Point(4, 10);
            this.lblSueldo.Name = "lblSueldo";
            this.lblSueldo.Size = new System.Drawing.Size(82, 13);
            this.lblSueldo.TabIndex = 0;
            this.lblSueldo.Text = "Sueldo mensual";
            // 
            // txtSueldo
            // 
            this.txtSueldo.Location = new System.Drawing.Point(93, 10);
            this.txtSueldo.Name = "txtSueldo";
            this.txtSueldo.Size = new System.Drawing.Size(100, 20);
            this.txtSueldo.TabIndex = 1;
            // 
            // gbxSueldo
            // 
            this.gbxSueldo.Controls.Add(this.txtSueldo);
            this.gbxSueldo.Controls.Add(this.lblSueldo);
            this.gbxSueldo.Location = new System.Drawing.Point(19, 25);
            this.gbxSueldo.Name = "gbxSueldo";
            this.gbxSueldo.Size = new System.Drawing.Size(206, 41);
            this.gbxSueldo.TabIndex = 2;
            this.gbxSueldo.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtNomina);
            this.groupBox1.Controls.Add(this.lblDias);
            this.groupBox1.Location = new System.Drawing.Point(19, 72);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(206, 41);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            // 
            // txtNomina
            // 
            this.txtNomina.Location = new System.Drawing.Point(93, 10);
            this.txtNomina.Name = "txtNomina";
            this.txtNomina.Size = new System.Drawing.Size(100, 20);
            this.txtNomina.TabIndex = 1;
            // 
            // lblDias
            // 
            this.lblDias.AutoSize = true;
            this.lblDias.Location = new System.Drawing.Point(4, 10);
            this.lblDias.Name = "lblDias";
            this.lblDias.Size = new System.Drawing.Size(67, 13);
            this.lblDias.TabIndex = 0;
            this.lblDias.Text = "Días nomina";
            // 
            // lblAguinaldo
            // 
            this.lblAguinaldo.AutoSize = true;
            this.lblAguinaldo.Location = new System.Drawing.Point(5, 7);
            this.lblAguinaldo.Name = "lblAguinaldo";
            this.lblAguinaldo.Size = new System.Drawing.Size(54, 13);
            this.lblAguinaldo.TabIndex = 4;
            this.lblAguinaldo.Text = "Aguinaldo";
            // 
            // lblRAguinaldo
            // 
            this.lblRAguinaldo.AutoSize = true;
            this.lblRAguinaldo.Location = new System.Drawing.Point(98, 7);
            this.lblRAguinaldo.Name = "lblRAguinaldo";
            this.lblRAguinaldo.Size = new System.Drawing.Size(13, 13);
            this.lblRAguinaldo.TabIndex = 5;
            this.lblRAguinaldo.Text = "$";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Vacaciones";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Infonavit";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 131);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "IMSS";
            // 
            // lblRVacaciones
            // 
            this.lblRVacaciones.AutoSize = true;
            this.lblRVacaciones.Location = new System.Drawing.Point(98, 45);
            this.lblRVacaciones.Name = "lblRVacaciones";
            this.lblRVacaciones.Size = new System.Drawing.Size(13, 13);
            this.lblRVacaciones.TabIndex = 9;
            this.lblRVacaciones.Text = "$";
            // 
            // lblRInfonavit
            // 
            this.lblRInfonavit.AutoSize = true;
            this.lblRInfonavit.Location = new System.Drawing.Point(98, 88);
            this.lblRInfonavit.Name = "lblRInfonavit";
            this.lblRInfonavit.Size = new System.Drawing.Size(13, 13);
            this.lblRInfonavit.TabIndex = 10;
            this.lblRInfonavit.Text = "$";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(236, 222);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(0, 13);
            this.label7.TabIndex = 11;
            // 
            // lblRIMSS
            // 
            this.lblRIMSS.AutoSize = true;
            this.lblRIMSS.Location = new System.Drawing.Point(98, 131);
            this.lblRIMSS.Name = "lblRIMSS";
            this.lblRIMSS.Size = new System.Drawing.Size(13, 13);
            this.lblRIMSS.TabIndex = 12;
            this.lblRIMSS.Text = "$";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(5, 172);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(29, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "RCV";
            // 
            // lblRRCV
            // 
            this.lblRRCV.AutoSize = true;
            this.lblRRCV.Location = new System.Drawing.Point(98, 172);
            this.lblRRCV.Name = "lblRRCV";
            this.lblRRCV.Size = new System.Drawing.Size(13, 13);
            this.lblRRCV.TabIndex = 14;
            this.lblRRCV.Text = "$";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblRRCV);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.lblRIMSS);
            this.groupBox2.Controls.Add(this.lblRInfonavit);
            this.groupBox2.Controls.Add(this.lblRVacaciones);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.lblRAguinaldo);
            this.groupBox2.Controls.Add(this.lblAguinaldo);
            this.groupBox2.Location = new System.Drawing.Point(19, 145);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 197);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.lblRSeguro);
            this.groupBox3.Controls.Add(this.lblRVales);
            this.groupBox3.Controls.Add(this.lblRSGMM);
            this.groupBox3.Controls.Add(this.lblSeguroV);
            this.groupBox3.Controls.Add(this.lblVales);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.lblRSueldo);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Location = new System.Drawing.Point(242, 145);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(200, 154);
            this.groupBox3.TabIndex = 16;
            this.groupBox3.TabStop = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(111, 172);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(0, 13);
            this.label11.TabIndex = 14;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(5, 172);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(0, 13);
            this.label12.TabIndex = 13;
            // 
            // lblRSeguro
            // 
            this.lblRSeguro.AutoSize = true;
            this.lblRSeguro.Location = new System.Drawing.Point(111, 131);
            this.lblRSeguro.Name = "lblRSeguro";
            this.lblRSeguro.Size = new System.Drawing.Size(13, 13);
            this.lblRSeguro.TabIndex = 12;
            this.lblRSeguro.Text = "$";
            // 
            // lblRVales
            // 
            this.lblRVales.AutoSize = true;
            this.lblRVales.Location = new System.Drawing.Point(111, 45);
            this.lblRVales.Name = "lblRVales";
            this.lblRVales.Size = new System.Drawing.Size(13, 13);
            this.lblRVales.TabIndex = 10;
            this.lblRVales.Text = "$";
            // 
            // lblRSGMM
            // 
            this.lblRSGMM.AutoSize = true;
            this.lblRSGMM.Location = new System.Drawing.Point(111, 88);
            this.lblRSGMM.Name = "lblRSGMM";
            this.lblRSGMM.Size = new System.Drawing.Size(13, 13);
            this.lblRSGMM.TabIndex = 9;
            this.lblRSGMM.Text = "$";
            // 
            // lblSeguroV
            // 
            this.lblSeguroV.AutoSize = true;
            this.lblSeguroV.Location = new System.Drawing.Point(5, 131);
            this.lblSeguroV.Name = "lblSeguroV";
            this.lblSeguroV.Size = new System.Drawing.Size(80, 13);
            this.lblSeguroV.TabIndex = 8;
            this.lblSeguroV.Text = "Seguro de Vida";
            // 
            // lblVales
            // 
            this.lblVales.AutoSize = true;
            this.lblVales.Location = new System.Drawing.Point(5, 45);
            this.lblVales.Name = "lblVales";
            this.lblVales.Size = new System.Drawing.Size(99, 13);
            this.lblVales.TabIndex = 7;
            this.lblVales.Text = "Vales de Despensa";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 88);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(40, 13);
            this.label18.TabIndex = 6;
            this.label18.Text = "SGMM";
            // 
            // lblRSueldo
            // 
            this.lblRSueldo.AutoSize = true;
            this.lblRSueldo.Location = new System.Drawing.Point(111, 7);
            this.lblRSueldo.Name = "lblRSueldo";
            this.lblRSueldo.Size = new System.Drawing.Size(13, 13);
            this.lblRSueldo.TabIndex = 5;
            this.lblRSueldo.Text = "$";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(5, 7);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(70, 13);
            this.label20.TabIndex = 4;
            this.label20.Text = "Sueldo Diario";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(76, 370);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 17;
            this.button1.Text = "Calcular";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Location = new System.Drawing.Point(212, 370);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(75, 23);
            this.btnLimpiar.TabIndex = 18;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.UseVisualStyleBackColor = true;
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtVacaciones);
            this.groupBox4.Controls.Add(this.lblVaciones);
            this.groupBox4.Location = new System.Drawing.Point(235, 25);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(214, 41);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            // 
            // txtVacaciones
            // 
            this.txtVacaciones.Location = new System.Drawing.Point(108, 10);
            this.txtVacaciones.Name = "txtVacaciones";
            this.txtVacaciones.Size = new System.Drawing.Size(100, 20);
            this.txtVacaciones.TabIndex = 1;
            // 
            // lblVaciones
            // 
            this.lblVaciones.AutoSize = true;
            this.lblVaciones.Location = new System.Drawing.Point(4, 10);
            this.lblVaciones.Name = "lblVaciones";
            this.lblVaciones.Size = new System.Drawing.Size(101, 13);
            this.lblVaciones.TabIndex = 0;
            this.lblVaciones.Text = "Dias de vacaciones";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(453, 425);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.btnLimpiar);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbxSueldo);
            this.Name = "Form1";
            this.Text = "Form1";
            this.gbxSueldo.ResumeLayout(false);
            this.gbxSueldo.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSueldo;
        private System.Windows.Forms.TextBox txtSueldo;
        private System.Windows.Forms.GroupBox gbxSueldo;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtNomina;
        private System.Windows.Forms.Label lblDias;
        private System.Windows.Forms.Label lblAguinaldo;
        private System.Windows.Forms.Label lblRAguinaldo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblRVacaciones;
        private System.Windows.Forms.Label lblRInfonavit;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblRIMSS;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblRRCV;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblRSeguro;
        private System.Windows.Forms.Label lblRVales;
        private System.Windows.Forms.Label lblRSGMM;
        private System.Windows.Forms.Label lblSeguroV;
        private System.Windows.Forms.Label lblVales;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label lblRSueldo;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtVacaciones;
        private System.Windows.Forms.Label lblVaciones;
    }
}

