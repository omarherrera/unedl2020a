﻿using System;

namespace DelegadoSencillo
{
    public delegate void Delegado(string d);

    class Program
    {
        static void Main(string[] args)
        {
            //Creacion y referencia del delegado
            Delegado del = new Delegado(CRadio.MetodoRadio);

            //Uso de la funcion por medio del delegado
            del("Qué onda?");
            
            //Creacion y referencia a otro delegado
            del = new Delegado(CPastel.MostrarPastel);

            //Uso de la funcion por medio del delegado
            del("Felicidades.");

        }
    }
}
