﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DelegadoSencillo
{
    class CRadio
    {
        //Metodo que actuara como delegado
        public static void MetodoRadio(string Mensaje)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Se encuentra en la clase radio.");
            Console.WriteLine("Este es el mensaje: {0}.", Mensaje);
        }
    }
}
