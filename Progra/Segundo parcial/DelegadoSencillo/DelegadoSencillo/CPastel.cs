﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DelegadoSencillo
{
    class CPastel
    {
        public static void MostrarPastel(string Anuncio)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("El pastel llevara el mensaje: {0}.", Anuncio);
        }
    }
}
