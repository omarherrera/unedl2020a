﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Chalanes
{
    class Program
    {
        private static Semaphore pooldechalanes;
        private static int _padding;
        private static int n = 0;
        private static int m = 0;
        private static int o = 0;
        private static int c = 0;
        private static int h = 0;
        private static int b = 0;
        private static void ChalanAlbanil(object chalan)
        {
            Console.WriteLine("Chalan de Albañil");
            Console.WriteLine("Thread {0}: {1}, Priority {2}",
                        Thread.CurrentThread.ManagedThreadId,
                        Thread.CurrentThread.ThreadState,
                        Thread.CurrentThread.Priority);
            Console.WriteLine("Chalan {0} esperando turno...", chalan);
            pooldechalanes.WaitOne();
            // A padding interval to make the output more orderly.
            int padding = Interlocked.Add(ref _padding, 100);
            Console.WriteLine("Chalan {0} obtiene turno...", chalan);
            Thread.Sleep(1000 + padding);
            Console.WriteLine("Chalan {0} termina turno...", chalan);
            Console.WriteLine("Chalan {0} libera su lugar {1}",
                chalan, pooldechalanes.Release());
        }

        public static async void ConstruirCasas(int cantidad, int Habitaciones, int baños)
        {
            Random rand = new Random();
            Task[] Casas = new Task[cantidad];
            int cont = 0;
            Action<object> action = (object obj) =>
            {
                Console.WriteLine(" La casa {0} ya esta lista para construir ", Task.CurrentId);
            };

            for (int i = 0; i < Casas.Length; i++)
            {

                Casas[i] = new Task(action, new string('1', i + 1));
                Console.WriteLine("Empezando casa {0} ", Task.CurrentId);
                if (cont != 0)
                {
                    Habitaciones = rand.Next(1, 10);
                    baños = rand.Next(1, 5);

                }
                for (int j = 0; j < Habitaciones; j++)
                {
                    Console.WriteLine(" creando habitaciones ");
                }
                for (int h = 0; h < baños; h++)
                {
                    Console.WriteLine(" creando baños");
                }
                Casas[i].Start();
                Thread.Sleep(500);
                cont++;


            }

            Console.WriteLine("Casas terminadas");

            await Task.WhenAll(Casas);

        }


        public static void Main(string[] args)
        {
            Console.WriteLine("¿Cuántos chalanes llegaron a chambear?");
            n = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("¿Cuántos pueden trabajar?");
            m = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("¿Cuántos segundos hay para contruir?");
            o = Convert.ToInt32(Console.ReadLine()) * 1000;

            Console.WriteLine("¿cuantas casas se van a construir?");
            c = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("¿cuantas habitaciones se van a construir?");
            h = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("¿cuantas baños se van a construir?");
            b = Convert.ToInt32(Console.ReadLine());

            pooldechalanes = new Semaphore(0, m);


            Console.WriteLine("Inicia la construcción");

            for (int i = 1; i <= m; i++)
            {
                Thread t = new Thread(new ParameterizedThreadStart(ChalanAlbanil));
                if (i % 2 == 0)
                {
                    t.Priority = ThreadPriority.AboveNormal;
                }
                else
                {
                    t.Priority = ThreadPriority.BelowNormal;
                }
                t.Start(i);
            }
            ConstruirCasas(c, h, b);

            Thread.Sleep(500);
            Console.WriteLine("Se libera un turno");
            pooldechalanes.Release(m);


            Thread.Sleep(o);
            Console.WriteLine("Se terminó la construcción");
        }
    }
}