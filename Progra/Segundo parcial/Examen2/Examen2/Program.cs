﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Chalanes
{
    class Program
    {
        private static Semaphore pooldechalanes;
        private static int _padding;
        private static int n = 0;
        private static int m = 0;
        private static int o = 0;
        private static int c = 0;
        private static int h = 0;
        private static int b = 0;

        public static void ConstruyeBaño()
        {
            Console.Write("Construyendo baño.");
        }

        public static void ConstruyeHabitacion()
        {
            Console.Write("Construyendo habitación.");
        }

        private static void ChalanAlbanil(object chalan)
        {
            Console.WriteLine("Chalan de Albañil");
            Console.WriteLine("Thread {0}: {1}, Priority {2}",
                        Thread.CurrentThread.ManagedThreadId,
                        Thread.CurrentThread.ThreadState,
                        Thread.CurrentThread.Priority);
            Console.WriteLine("Chalan {0} esperando turno...", chalan);
            pooldechalanes.WaitOne();
            // A padding interval to make the output more orderly.
            int padding = Interlocked.Add(ref _padding, 100);
            Console.WriteLine("Chalan {0} obtiene turno...", chalan);
            Thread.Sleep(1000 + padding);
            Console.WriteLine("Chalan {0} termina turno...", chalan);
            Console.WriteLine("Chalan {0} libera su lugar {1}",
                chalan, pooldechalanes.Release());
        }

        public static void ConstruyeCasa(int num, int h, int b)
        {
            Random rnd = new Random();
            Task[] Casas = new Task[num];

            for (int i = 0; i < Casas.Length; i++)
            {

                Casas[i] = Task.Run(() => Thread.Sleep(rnd.Next(500, 2000)));
                Console.WriteLine("Empezando casa {0} ", i+1);

                for (int j = 0; j < h; j++)
                {
                    ConstruyeHabitacion();
                }
                for (int k = 0; k < b; k++)
                {
                    ConstruyeBaño();
                }
                //Casas[i].Start();
                Thread.Sleep(500);

            }

            Console.WriteLine("Casas terminadas");

        }


        public static void Main(string[] args)
        {
            n = 20;
            Console.WriteLine("Chalanes disponibles: "+n);
            
            Console.WriteLine("¿Cuántos pueden trabajar?");
            m = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("¿Cuántos segundos hay para contruir?");
            o = Convert.ToInt32(Console.ReadLine()) * 1000;

            Console.WriteLine("Casas que se van a construir: ");
            c = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Habitaciones por casa: ");
            h = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Baños por casa: ");
            b = Convert.ToInt32(Console.ReadLine());

            pooldechalanes = new Semaphore(0, m);


            Console.WriteLine("Inicia la construcción");

            for (int i = 1; i <= m; i++)
            {
                Thread t = new Thread(new ParameterizedThreadStart(ChalanAlbanil));
                if (i % 2 == 0)
                {
                    t.Priority = ThreadPriority.AboveNormal;
                }
                else
                {
                    t.Priority = ThreadPriority.BelowNormal;
                }
                t.Start(i);
            }
            ConstruyeCasa(c, h, b);

            Thread.Sleep(500);
            Console.WriteLine("Se libera un turno");
            pooldechalanes.Release(m);


            Thread.Sleep(o);
            Console.WriteLine("Se terminó la construcción");
        }
    }
}